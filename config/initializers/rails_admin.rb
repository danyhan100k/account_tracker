RailsAdmin.config do |config|

  ### Popular gems integration

  ## == Devise ==
  # config.authenticate_with do
  #   warden.authenticate! scope: :user
  # end
  # config.current_user_method(&:current_user)

  ## == Cancan ==
  # config.authorize_with :cancan

  ## == Pundit ==
  # config.authorize_with :pundit

  ## == PaperTrail ==
  # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app

    ## With an audit adapter, you can add:
    # history_index
    # history_show
  end

  config.model 'SalesRep' do
    edit do
      field :first_name
      field :last_name
      field :email
      field :is_manager do
        partial :is_manager
      end
      field :manager do
        inline_add false
        inline_edit false
      end
      field :manager_title
      # TODO: Embed address form
      field :address do
        inline_add true
      end
      field :phone_numbers
    end
  end

  config.model 'PanelAdjustmentName' do
    configure :panel_adjustments do
      hide
    end
  end

  #Prefix
  #Company Name
  #Account Numbers
  #Providers
  #Sales Rep
  #Testing Options (Panels/customizations)
  #Report Delivery Type
  config.model 'Account' do
    field :prefix
    field :company_name
    field :account_numbers

    field :providers do
      inline_add true
    end

    field :sales_rep do
      inline_add false
      inline_edit false
    end

    field :base_panels

    field :report_delivery_types do
      inline_add false
    end

    field :requisition_ranges
    field :has_sample_collector
    field :notes

  end

  config.model 'AccountProvider' do 
    visible false
  end
  config.model 'AccountReportDeliveryType' do
    visible false
  end

  config.model 'AccountNumber' do
    visible false
  end

  config.model 'Address' do
    visible false
    configure :addressable do
      hide
    end
  end

  config.model 'BasePanel' do
    # visible false
    configure :base_panel_name do
      inline_add false
      inline_edit false
    end
    configure :account do
      inline_add false
      inline_edit false
    end
  end

  config.model 'BasePanelName' do
    configure :base_panels do
      hide
    end
  end

  config.model 'PhoneNumber' do
    visible false
    configure :phoneable do
      hide
    end
    configure(:number) do
      html_attributes do
        html_attributes.merge({
          'data-phone-number' => true
        })
      end
    end
  end

  config.model 'Provider' do
    configure :accounts do
      hide
    end
    configure :account_providers do 
      hide
    end
  end

  config.model 'ReportDeliveryType' do
    configure :accounts do
      hide
    end
  end

  config.model 'Specimen' do
    configure :account do
      inline_add false
      inline_edit false
    end
  end

  config.model 'PanelAdjustment' do
    # visible false
    configure :base_panel do
      # hide
      inline_add false
      inline_edit false
    end
    configure :panel_adjustment_name do 
      inline_add false
      inline_edit false
    end
  end

  config.model 'RequisitionRange' do
    field :starts_at
    field :ends_at
    field :account do
      inline_add false
      inline_edit false
    end
  end
end
