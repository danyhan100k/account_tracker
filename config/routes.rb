Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  root to: 'accounts#index'
  resources :sales_reps, only: [:index, :show]

  resources :accounts, only: :index do 
    collection do 
      post(:import)
      get(:accounts_search_table)
    end
  end

  resources :specimens, only: [] do 
    collection do
      get 'accession_log', to: 'specimens#accession_log', as: 'accession_log'
      get 'eia_report/pick_a_date', to: 'specimens#pick_a_date', as: 'pick_a_date'
      get 'eia_report/generate_report', to: 'specimens#generate_report'
      get(:generate_report)
      get(:check_for_report)
    end
  end

  namespace :api, :defaults => { format: :json } do 
    namespace :v1 do
      get 'requisition/:id' => 'accounts#show'
      resources :accounts, only: :index
      resources :specimens, only: [:index, :create, :update]
    end
    # namespace :v2 do 
    # end
  end
end

# == Route Map
#
#      Prefix Verb URI Pattern Controller#Action
# rails_admin      /admin      RailsAdmin::Engine
#
# Routes for RailsAdmin::Engine:
#   dashboard GET         /                                      rails_admin/main#dashboard
#       index GET|POST    /:model_name(.:format)                 rails_admin/main#index
#         new GET|POST    /:model_name/new(.:format)             rails_admin/main#new
#      export GET|POST    /:model_name/export(.:format)          rails_admin/main#export
# bulk_delete POST|DELETE /:model_name/bulk_delete(.:format)     rails_admin/main#bulk_delete
# bulk_action POST        /:model_name/bulk_action(.:format)     rails_admin/main#bulk_action
#        show GET         /:model_name/:id(.:format)             rails_admin/main#show
#        edit GET|PUT     /:model_name/:id/edit(.:format)        rails_admin/main#edit
#      delete GET|DELETE  /:model_name/:id/delete(.:format)      rails_admin/main#delete
# show_in_app GET         /:model_name/:id/show_in_app(.:format) rails_admin/main#show_in_app
#
