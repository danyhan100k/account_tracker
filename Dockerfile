# Dockerfile
FROM quay.io/aptible/ruby:ruby-2.2

RUN apt-get update && apt-get -y install build-essential

# 
# System prerequisites
RUN apt-get update && apt-get -y install libpq-dev

# If you require additional OS dependencies, install them here:
# RUN apt-get update && apt-get -y install imagemagick nodejs
RUN apt-get update && apt-get -y install nodejs libmysqlclient-dev
# test install mysql client to connect
#RUN apt-get -y install mysql-client

ENV INSTALL_PATH /app
RUN mkdir $INSTALL_PATH


# This sets the context of where commands will be ran in and is documented
# on Docker's website extensively.
WORKDIR $INSTALL_PATH

# Ensure gems are cached and only get updated when they change. This will
# drastically increase build times when your gems do not change.
COPY Gemfile Gemfile
COPY Gemfile.lock Gemfile.lock
RUN bundle install

COPY . . 

RUN cp config/database.yml.sample config/database.yml 

RUN RAILS_ENV=production bundle exec rake assets:precompile
RUN RAILS_ENV=production bundle exec rake webpack:compile
ENV PORT 3002 
EXPOSE 3002 

