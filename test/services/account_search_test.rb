require 'test_helper'

class AccountSearchTest < ActiveSupport::TestCase
  test "finds accounts searching on accounts.prefix" do
    another_account = create(:account, prefix: "DEF")
    account = create(:account, prefix: "ABC")
    assert_equal [account], AccountSearch.new(terms: "ab", search_by: "accounts").accounts
  end

  test "finds accounts searching on accounts.company_name" do
    another_account = create(:account, company_name: "Another company")
    account = create(:account, company_name: "My Company")
    assert_equal [account], AccountSearch.new(terms: "my", search_by: "accounts").accounts
  end

  test "finds accounts searching on account_numbers" do
    another_account = create(
      :account,
      account_numbers: [
        create(:account_number, number: "123456")
      ]
    )
    account = create(
      :account,
      account_numbers: [
        create(:account_number, number: "654321")
      ]
    )
    assert_equal [account], AccountSearch.new(terms: "654", search_by: "accounts").accounts
  end

  test "finds accounts searching on addresses" do
    another_account = create(
      :account,
      account_numbers: [
        create(
          :account_number,
          addresses: [
            create(:address, city: "New York")
          ]
        )
      ]
    )
    account = create(
      :account,
      account_numbers: [
        create(
          :account_number,
          addresses: [
            create(:address, city: "Chicago")
          ]
        )
      ]
    )
    assert_equal [account], AccountSearch.new(terms: "chi", search_by: "addresses").accounts
  end

  test "finds accounts searching on providers" do
    another_account = create(
      :account,
      providers: [
        create(:provider, name: "Dr. Mario")
      ]
    )
    account = create(
      :account,
      providers: [
        create(:provider, name: "Dr. House")
      ]
    )
    assert_equal [account], AccountSearch.new(terms: "hou", search_by: "providers").accounts
  end

  test "finds accounts searching on sales_reps" do
    another_account = create(
      :account,
      sales_rep: create(
        :sales_rep,
        first_name: "John"
      )
    )
    account = create(
      :account,
      sales_rep: create(
        :sales_rep,
        first_name: "Mary"
      )
    )
    assert_equal [account], AccountSearch.new(terms: "mar", search_by: "sales_reps").accounts
  end

  test "finds accounts searching on sales_reps full name" do
    skip "Still need to find a way to implement that"
  end

  test "finds accounts searching on phone_numbers through account" do
    another_account = create(
      :account,
      account_numbers: [
        create(
          :account_number,
          phone_numbers: [
            create(:phone_number, number: "(999) 999-9999")
          ]
        )
      ]
    )
    account = create(
      :account,
      account_numbers: [
        create(
          :account_number,
          phone_numbers: [
            create(:phone_number, number: "(888) 888-8888")
          ]
        )
      ]
    )
    assert_equal [account], AccountSearch.new(terms: "888", search_by: "phone_numbers").accounts
  end

  test "finds accounts searching on phone_numbers through sales rep" do
    another_account = create(
      :account,
      sales_rep: create(
        :sales_rep,
        phone_numbers: [
          create(:phone_number, number: "(999) 999-9999")
        ]
      )
    )
    account = create(
      :account,
      sales_rep: create(
        :sales_rep,
        phone_numbers: [
          create(:phone_number, number: "(888) 888-8888")
        ]
      )
    )
    assert_equal [account], AccountSearch.new(terms: "888", search_by: "phone_numbers").accounts
  end

  test "finds accounts searching on base_panel_names" do
    another_account = create(
      :account,
      base_panels: [
        create(
          :base_panel,
          base_panel_name: create(
            :base_panel_name,
            name: "illness"
          )
        )
      ]
    )
    account = create(
      :account,
      base_panels: [
        create(
          :base_panel,
          base_panel_name: create(
            :base_panel_name,
            name: "sickness"
          )
        )
      ]
    )
    assert_equal [account], AccountSearch.new(terms: "sick", search_by: "base_panels").accounts
  end

  test "finds accounts searching on panel_adjustment_names" do
    another_account = create(
      :account,
      base_panels: [
        create(
          :base_panel,
          panel_adjustments: [
            create(
              :panel_adjustment,
              panel_adjustment_name: create(
                :panel_adjustment_name,
                name: "Substance"
              )
            )
          ]
        )
      ]
    )
    account = create(
      :account,
      base_panels: [
        create(
          :base_panel,
          panel_adjustments: [
            create(
              :panel_adjustment,
              panel_adjustment_name: create(
                :panel_adjustment_name,
                name: "Chemical"
              )
            )
          ]
        )
      ]
    )
    assert_equal [account], AccountSearch.new(terms: "chem", search_by: "base_panels").accounts
  end

  test "finds accounts searching on report_delivery_types" do
    another_account = create(
      :account,
      report_delivery_types: [
        create(
          :report_delivery_type,
          name: "Fax"
        )
      ]
    )
    account = create(
      :account,
      report_delivery_types: [
        create(
          :report_delivery_type,
          name: "Online"
        )
      ]
    )
    assert_equal [account], AccountSearch.new(terms: "on", search_by: "report_delivery_types").accounts
  end
end
