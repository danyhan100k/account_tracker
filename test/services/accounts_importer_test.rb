require 'test_helper'

class AccountsImporterTest < ActiveSupport::TestCase
  ROW_SEP = "$"
  HEADERS = [' '] * 13
  TEST_MODE = "OFF"

setup do 
  skip("Skipped Accounts Importer Tests") if TEST_MODE == "OFF"
end

  test "finds the account after sanitizing the identifiers" do
    data = [
      HEADERS,
      [' ', "  <html>#{account.prefix} </html> ", account_number.number, ' ', ' ', "Mr. Provider", "123456", ' ', ' ', ' ', ' ', ' ', ' ']
    ]

    import data
  end

  test "creates a provider associated with the account" do
    data = [
      HEADERS,
      [' ', account.prefix, account_number.number, ' ', ' ', "Mr. Provider", "123456", ' ', ' ', ' ', ' ', ' ', ' ']
    ]

    import data
    provider = Provider.last

    assert_equal "Mr. Provider", provider.name
    assert_equal "123456", provider.npi_number
  end

  test "creates more than one provider associated with the account" do
    data = [
      HEADERS,
      [' ', account.prefix, account_number.number, ' ', ' ', "Mr. Provider\nMs. Provider", "123456\n654321", ' ', ' ', ' ', ' ', ' ', ' ']
    ]

    import data
    provider = Provider.find_by! name: "Mr. Provider"
    assert_equal "123456", provider.npi_number

    provider = Provider.find_by! name: "Ms. Provider"
    assert_equal "654321", provider.npi_number
  end

  test "strips the whitespaces from around the provider fields" do
    data = [
      HEADERS,
      [' ', account.prefix, account_number.number, ' ', ' ', "  Mr. Provider   ", "  123456  ", ' ', ' ', ' ', ' ', ' ', ' ']
    ]

    import data
    provider = Provider.last

    assert_equal "Mr. Provider", provider.name
    assert_equal "123456", provider.npi_number
  end

  test "finds the account numbers" do
    account_number_1 = create(:account_number, account: account, number: "123456")
    account_number_2 = create(:account_number, account: account, number: "123456-A1")
    data = [
      HEADERS,
      [' ', account.prefix, "  <html> Req# \n 123456  \n  Los Angeles:  \n  123456 -A1  </html> " , ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ']
    ]

    import data
  end

  test "creates phone numbers for the respective account numbers" do
    account_number_1 = create(:account_number, account: account)
    account_number_2 = create(:account_number, account: account)
    data = [
      HEADERS,
      [' ', account.prefix, [account_number_1.number, account_number_2.number].join("\n"), ' ', ' ', ' ', ' ', "(000) 000-0001\nFirst note\n(000)  000.0002\nSecond note   (000) 000-0003", "  Third note  \n  (000)000-0004  ?  ", ' ', ' ', ' ', ' ']
    ]

    import data

    phone_number = PhoneNumber.find_by! number: "(000) 000-0001"
    assert_equal "First note", phone_number.notes
    assert_equal account_number_1, phone_number.phoneable
    assert_equal "phone number", phone_number.number_type

    phone_number = PhoneNumber.find_by! number: "(000)  000.0002"
    assert_equal "Second note", phone_number.notes
    assert_equal account_number_2, phone_number.phoneable
    assert_equal "phone number", phone_number.number_type

    phone_number = PhoneNumber.find_by! number: "(000) 000-0003"
    assert_equal "", phone_number.notes
    assert_equal account_number_1, phone_number.phoneable
    assert_equal "phone number", phone_number.number_type

    phone_number = PhoneNumber.find_by! number: "(000)000-0004"
    assert_equal "Third note", phone_number.notes
    assert_equal account_number_1, phone_number.phoneable
    assert_equal "fax", phone_number.number_type

    phone_number = PhoneNumber.find_by! notes: "?"
    assert_nil phone_number.number
    assert_equal account_number_2, phone_number.phoneable
    assert_equal "fax", phone_number.number_type
  end

  test "creates a sales rep" do
    data = [
      HEADERS,
      [' ', account.prefix, account_number.number, ' ', ' ', ' ', ' ', ' ', ' ', '  John  F.  Doe  ', ' ', ' ', ' ']
    ]

    import data

    sales_rep = SalesRep.last
    assert_equal "John F.", sales_rep.first_name
    assert_equal "Doe", sales_rep.last_name
    assert_equal [account], sales_rep.accounts
  end

  test "assigns first name to sales rep with only one name" do
    data = [
      HEADERS,
      [' ', account.prefix, account_number.number, ' ', ' ', ' ', ' ', ' ', ' ', '  John  ', ' ', ' ', ' ']
    ]

    import data

    sales_rep = SalesRep.last
    assert_equal "John", sales_rep.first_name
    assert_nil sales_rep.last_name
    assert_equal [account], sales_rep.accounts
  end

  test "doesn't create a sales rep if the value to import is an integer" do
    data = [
      HEADERS,
      [' ', account.prefix, account_number.number, ' ', ' ', ' ', ' ', ' ', ' ', 0, ' ', ' ', ' ']
    ]

    import data

    assert_equal 0, SalesRep.count
  end

  test "doesn't create a sales rep if the value to import is nil" do
    data = [
      HEADERS,
      [' ', account.prefix, account_number.number, ' ', ' ', ' ', ' ', ' ', ' ', nil, ' ', ' ', ' ']
    ]

    import data

    assert_equal 0, SalesRep.count
  end

  test "doesn't create a sales rep if the value to import is an empty string" do
    data = [
      HEADERS,
      [' ', account.prefix, account_number.number, ' ', ' ', ' ', ' ', ' ', ' ', "\\", ' ', ' ', ' ']
    ]

    import data

    assert_equal 0, SalesRep.count
  end

  test "creates the base panels and panel adjustments" do
    assert_equal 0, PanelAdjustmentName.count
    assert_equal 0, BasePanel.count

    base_panel_cell = "   
   <html>PGX    
   Customized Pain Management Panel   
   (with modifications)  
    Yes: RITALINIC ACID  
    NO: NORTRIPTYLINE  
    </html>  "

    data = [
      HEADERS,
      [' ', account.prefix, account_number.number, ' ', ' ', ' ', ' ', ' ', ' ', ' ', base_panel_cell, ' ', ' ']
    ]

    import data

    base_panel = BasePanel.last!
    assert_equal account, base_panel.account
    assert_equal "PGX Customized Pain Management Panel (with modifications)", base_panel.name

    panel_adjustment_name = PanelAdjustmentName.find_by! name: "RITALINIC ACID"
    panel_adjustment = base_panel.panel_adjustments.find_by! panel_adjustment_name: panel_adjustment_name
    assert_equal "YES", panel_adjustment.qualifier

    panel_adjustment_name = PanelAdjustmentName.find_by! name: "NORTRIPTYLINE"
    panel_adjustment = base_panel.panel_adjustments.find_by! panel_adjustment_name: panel_adjustment_name
    assert_equal "NO", panel_adjustment.qualifier
  end

  test "creates the report delivery types" do
    data = [
      HEADERS,
      [' ', account.prefix, account_number.number, ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', "   Fax   \n  \n  Online Client Login:  "]
    ]

    import data

    assert_equal 2, ReportDeliveryType.count

    report_delivery_type = ReportDeliveryType.find_by! name: 'Fax'
    assert_equal [account], report_delivery_type.accounts

    report_delivery_type = ReportDeliveryType.find_by! name: 'Online Client Login'
    assert_equal [account], report_delivery_type.accounts
  end

  test "import is rolled back if there's any error" do
    # Trying to create a phone number without an account number
    data = [
      HEADERS,
      [' ', account.prefix, ' ', ' ', ' ', ' ', ' ', "(000) 000-0001", ' ', ' ', ' ', ' ', ' ']
    ]

    begin
      import data
      raise "Importing the data should have raised an error"
    rescue AccountsImporter::ImportError => e
      assert_equal 1, e.errors.length
      assert_equal "Must provide at least 1 account number", e.errors.first[:error][:message]
      assert_equal 0, Provider.count
      assert_equal 0, PhoneNumber.count
    end
  end

  private

    def account
      @account ||= create(:account, sales_rep: nil)
    end

    def account_number
      @account_number ||= create(:account_number, account: account)
    end

    def import(data)
      AccountsImporter.new.import(data)
    end
end
