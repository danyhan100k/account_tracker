require 'test_helper'

class API::V1::AccountsControllerTest < ActionController::TestCase

	test "account found by given requisition range" do
		requisition_range = create(:requisition_range,
															 starts_at: 1,
															 ends_at: 10,
															 account: create_account)

		response = get(:index, { id: rand(requisition_range.starts_at..requisition_range.ends_at)})

		account_prefix_regexp = set_prefix_regexp requisition_range.account.prefix
		assert_equal create_account.prefix, response.body.match(account_prefix_regexp)[0]
		assert_response(200, "OK")		
	end

	test "account not found by given requisition range" do
		requisition_range = create(:requisition_range,
																starts_at: 20,
																ends_at: 50,
																account: create_account)
		response = get(:index,{ id: 17})

		account_prefix_regexp = set_prefix_regexp requisition_range.account.prefix
		assert_equal "errors", response.body.match(/errors/)[0]
		assert_nil response.body.match(account_prefix_regexp)
		assert_response(422, "Unprocessable Entity")
	end

	def create_account
		@account ||= create(:account, prefix: "A2")
	end

	def set_prefix_regexp(prefix)
		Regexp.new prefix
	end

end
