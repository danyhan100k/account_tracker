ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require 'capybara/rails'
require 'capybara/poltergeist'
require 'database_cleaner'
Capybara.default_driver = :poltergeist
Capybara.javascript_driver = :poltergeist

class ActiveSupport::TestCase
  include FactoryGirl::Syntax::Methods
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  # fixtures :all

  # Add more helper methods to be used by all tests here...

  def errors_when(model, attribute, value)
    model.send("#{attribute}=", value)
    model.valid?
    model.errors[attribute]
  end
end

class ActionDispatch::IntegrationTest
  include Capybara::DSL
  self.use_transactional_fixtures = false

  setup do
    DatabaseCleaner.strategy = :truncation
    DatabaseCleaner.start
  end

  # Reset sessions and driver between tests
  # Use super wherever this method is redefined in your individual test classes
  teardown do
    DatabaseCleaner.clean
    Capybara.reset_sessions!
    Capybara.use_default_driver
  end

  def select_from_autocomplete(model, association, value)
    find("##{model}_#{association}_id_field .dropdown-toggle").trigger('click')
    find(".ui-menu-item a", text: value).trigger('click')
  end

  def select_from_multiselect(selector, value)
    within(selector) do
      find('.ra-multiselect-left .ra-multiselect-collection').select(value)
      find_link('Add').trigger('click')
    end
  end
end
