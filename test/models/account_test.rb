require 'test_helper'

class AccountTest < ActiveSupport::TestCase
  test "name is delegated to company_name" do
    account.company_name = "Some company name"
    assert_equal "#{account.prefix} - #{account.company_name}", account.name
  end

  test "doesn't accept empty company_name" do
    assert_equal ["can't be blank"], errors_when(account, :company_name, '')
  end

  test "doesn't accept empty prefix" do
    assert_equal ["can't be blank"], errors_when(account, :prefix, '')
  end

  test "doesn't accept empty sales_rep" do
    # assert_equal ["can't be blank"], errors_when(account, :sales_rep, nil)
  end

  test "doesn't accept company_name longer than 255 characters" do
    assert_equal ["is too long (maximum is 255 characters)"], errors_when(account, :company_name, '*' * 256)
  end

  test "doesn't accept prefix longer than 255 characters" do
    assert_equal ["is too long (maximum is 255 characters)"], errors_when(account, :prefix, '*' * 256)
  end

  test "doesn't accept notes longer than 255 characters" do
    assert_equal ["is too long (maximum is 255 characters)"], errors_when(account, :notes, '*' * 256)
  end


  test "belongs to SalesRep" do
    account.save!
    sales_rep = create(:sales_rep, manager_title: "manager")
    account.sales_rep = sales_rep
    assert_equal sales_rep.id, account.sales_rep_id
  end

  test "has many AccountProviders" do
    account.save!
    account_provider = create(:account_provider)
    account.account_providers << account_provider
    assert_equal [account_provider], account.account_providers
  end

  test "has many Providers through AccountProviders" do
    account.save!
    provider = create(:provider)
    account_provider = create(:account_provider, provider: provider)
    account.account_providers << account_provider
    assert_equal [provider], account.providers
  end

  test "has many BasePanels dependent destroy" do
    account.save!
    base_panel = create(:base_panel)
    account.base_panels << base_panel
    assert_equal [base_panel], account.base_panels

    account.destroy
    assert_not BasePanel.exists?(base_panel.id)
  end

  test "has many AccountNumbers dependent destroy" do
    account.save!
    account_number = create(:account_number)
    account.account_numbers << account_number
    assert_equal [account_number], account.account_numbers

    account.destroy
    assert_not AccountNumber.exists?(account_number.id)
  end

  test "has many AccountReportDeliveryTypes" do
    account.save!
    account_report_delivery_type = create(:account_report_delivery_type)
    account.account_report_delivery_types << account_report_delivery_type
    assert_equal [account_report_delivery_type], account.account_report_delivery_types
  end

  test "has many ReportDeliveryTypes through AccountReportDeliveryTypes" do
    account.save!
    report_delivery_type = create(:report_delivery_type)
    account_report_delivery_type = create(:account_report_delivery_type, report_delivery_type: report_delivery_type)
    account.account_report_delivery_types << account_report_delivery_type
    assert_equal [report_delivery_type], account.report_delivery_types
  end

  test "has many PhoneNumbers" do
    account.save!
    account_number = create(:account_number)
    phone_number = create(:phone_number)
    account.account_numbers << account_number
    account_number.phone_numbers << phone_number
    assert_equal [phone_number], account.phone_numbers
  end

  private

  def account
    @account ||= build(:account)
  end
end
