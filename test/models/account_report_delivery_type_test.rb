require 'test_helper'

class AccountReportDeliveryTypeTest < ActiveSupport::TestCase
 test "does not accept empty account" do 
   	acc_report_delivery_type = new_account_report_delivery_type
   	assert_equal ["can't be blank"], errors_when(acc_report_delivery_type, :account, nil)
   end

	 test "does not accept empty report delivery type" do 
	 	acc_report_delivery_type = new_account_report_delivery_type
	 	assert_equal ["can't be blank"], errors_when(acc_report_delivery_type, :report_delivery_type, nil)
	 end

	 test "belongs to account" do 
	 	account = create(:account)
	 	acc_report_delivery_type = new_account_report_delivery_type
	 	acc_report_delivery_type.assign_attributes(account: account)
	 	acc_report_delivery_type.save!
	 	assert_equal account.id, acc_report_delivery_type.account_id
	 end

	 test "belongs to report delivery type" do 
	 	report_delivery_type = create(:report_delivery_type)
	 	acc_report_delivery_type = new_account_report_delivery_type
	 	acc_report_delivery_type.assign_attributes(report_delivery_type: report_delivery_type)
	 	acc_report_delivery_type.save!
	 	assert_equal report_delivery_type.id, acc_report_delivery_type.report_delivery_type_id
	 end

private
	def new_account_report_delivery_type
		@report_delivery_type ||= build(:account_report_delivery_type)
	end
end
