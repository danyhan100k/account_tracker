require 'test_helper'

class PanelAdjustmentNameTest < ActiveSupport::TestCase
  test "doesn't accept empty name" do
    assert_equal ["can't be blank"], errors_when(panel_adjustment_name, :name, '')
  end

  test "accepts name longer than 255" do
    panel_adjustment_name.name = "a" * 256
    panel_adjustment_name.save!
  end

  test "has many panel adjustments" do 
    panel_adjustment = create(:panel_adjustment)
    panel_adjustment_name.panel_adjustments << panel_adjustment
    assert_equal [panel_adjustment], panel_adjustment_name.panel_adjustments
  end

  private

  def panel_adjustment_name
    @panel_adjustment_name ||= build(:panel_adjustment_name)
  end
end
