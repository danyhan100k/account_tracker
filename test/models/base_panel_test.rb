require 'test_helper'

class BasePanelTest < ActiveSupport::TestCase

  test "doesn't accept empty base panel name" do
    assert_equal ["can't be blank"], errors_when(new_base_panel, :base_panel_name, nil)
  end

  test "doesn't accept empty account" do
    # assert_equal ["can't be blank"], errors_when(new_base_panel, :account, nil)
  end

  test "belongs to BasePanelName" do
    new_base_panel.save!
    base_panel_name = create(:base_panel_name)
    new_base_panel.base_panel_name = base_panel_name
    assert_equal base_panel_name.id, new_base_panel.base_panel_name_id
  end

  test "belongs to Account" do
    new_base_panel.save!
    account = create(:account)
    new_base_panel.account = account
    assert_equal account.id, new_base_panel.account_id
  end

  test "has many TestPanels" do
    new_base_panel.save!
    panel_adjustment = create(:panel_adjustment)
    new_base_panel.panel_adjustments << panel_adjustment
    assert_equal [panel_adjustment], new_base_panel.panel_adjustments
  end

  private
  def new_base_panel
  	@base_panel ||= build(:base_panel)
  end
end
