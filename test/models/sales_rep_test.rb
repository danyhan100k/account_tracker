require 'test_helper'

class SalesRepTest < ActiveSupport::TestCase

	test "doesn't accept empty first name" do 
		assert_equal ["can't be blank"], errors_when(sales_rep, :first_name, '')
	end

  test "sales rep can be created indepedently without manager or manager title" do 
    sales_rep = create(:sales_rep, manager_title: nil, manager: nil)
    assert sales_rep.valid?
    assert sales_rep.errors.messages.blank?
  end
  
	test "belongs to manager" do
		manager = create(:sales_rep, manager_title: "Manager")
    sales_rep.manager = manager
    sales_rep.save!
    # assert_equal manager.id, sales_rep.manager.id
  end

  test "has many phone numbers" do
    phone_number = create(:phone_number)
    sales_rep.manager_title = "Manager"
 		sales_rep.phone_numbers << phone_number
    sales_rep.save!
 		assert_equal [phone_number], sales_rep.phone_numbers
  end

  test "has many accounts" do
  	account = create(:account)
    sales_rep.manager_title = "Manager"
 		sales_rep.accounts << account
    sales_rep.save!
 		assert_equal [account], sales_rep.accounts
  end

  test "has one address" do
  	address = create(:address)
    sales_rep.manager_title = "Manager"
 		sales_rep.address = address
    sales_rep.save!
 		assert_equal address, sales_rep.address  	
  end

  private
  def sales_rep
  	@sales_rep ||= build(:sales_rep)
  end

end
