require 'test_helper'

class RequisitionRangeTest < ActiveSupport::TestCase
  test "starts_at must be smaller or equals than the ends_at" do
    requisition_range = build(:requisition_range, ends_at: 1)
    assert_equal [], errors_when(requisition_range, :starts_at, 0)
    assert_equal [], errors_when(requisition_range, :starts_at, 1)
    assert_equal(
      ["starts_at must be less than or equals than ends_at"],
      errors_when(requisition_range, :starts_at, 2)
    )
  end

  test "it doesn't overlap with another range in the database" do
    create(:requisition_range, starts_at: 1, ends_at: 3)
    requisition_range = build(:requisition_range)

    requisition_range.ends_at = 4
    assert_equal [], errors_when(requisition_range, :starts_at, 4)

    requisition_range.ends_at = 4
    $FOCUS = true
    assert_equal ["range is taken"], errors_when(requisition_range, :starts_at, 3)

    requisition_range.starts_at = 0
    assert_equal ["range is taken"], errors_when(requisition_range, :ends_at, 1)

    requisition_range.starts_at = 2
    assert_equal ["range is taken"], errors_when(requisition_range, :ends_at, 2)

    requisition_range.ends_at = 4
    assert_equal ["range is taken"], errors_when(requisition_range, :starts_at, 0)

    requisition_range.starts_at = 0
    assert_equal [], errors_when(requisition_range, :ends_at, 0)
  end

  test "it doesn't overlap with itself when updating" do
    requisition_range = create(:requisition_range, starts_at: 1, ends_at: 3)
    assert_equal [], errors_when(requisition_range, :starts_at, 2)
  end
end
