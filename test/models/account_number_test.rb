require 'test_helper'

class AccountNumberTest < ActiveSupport::TestCase
  setup do
    @account_number_attr = attributes_for(:account_number)
    @acc_number = AccountNumber.new
  end

	test "is valid with valid attributes" do
		account = create(:account)
		address = create(:address)
		@acc_number.number = 7777777
		@acc_number.notes = "long term customer"
		@acc_number.account = account
		@acc_number.addresses << address
		@acc_number.save
		assert @acc_number.errors.blank?
	end

 	test "is not valid without a number" do
 		account_number_attr = @account_number_attr.merge({number: ""}) 
 		account = build(:account_number, account_number_attr)
 		account.save
 		assert_equal ["can't be blank"], account.errors[:number]
 	end

 	test "is not valid without an account" do 
 		account_number_attr = @account_number_attr.merge({ account: nil })
 		account_number = build(:account_number, account_number_attr)
 		account_number.save
 		assert_equal ["can't be blank"], account_number.errors[:account]
 	end

 	test "account can have many addresses" do 
 		account_number = create(:account_number_with_many_addresses)
 		assert account_number.addresses.length > 1
 	end
end
