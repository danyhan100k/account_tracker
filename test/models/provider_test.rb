require 'test_helper'

class ProviderTest < ActiveSupport::TestCase

  test "doesn't accept empty name" do
    assert_equal ["can't be blank"], errors_when(provider, :name, '')
  end

  test "doesn't accept empty npi_number" do
    # assert_equal ["can't be blank"], errors_when(provider, :npi_number, '')
  end

  test "doesn't accept invalid npi_number" do
    #TODO: Need to create test after format for npi number is set
  end

  test "doesn't accept name longer than 255 characters" do
    assert_equal ["is too long (maximum is 255 characters)"], errors_when(provider, :name, '*' * 256)
  end

  test "doesn't accept npi_number longer than 255 characters" do
    assert_equal ["is too long (maximum is 255 characters)"], errors_when(provider, :npi_number, '*' * 256)
  end
  test "has many AccountProviders" do
    provider.save!
    account_provider = create(:account_provider)
    provider.account_providers << account_provider
    assert_equal provider, account_provider.reload.provider
  end

  test "has many Accounts through AccountProviders" do
    provider.save!
    account = create(:account)
    account_provider = create(:account_provider, account: account)
    provider.account_providers << account_provider
    assert_equal [account], provider.accounts
  end

  private

  def provider
    @provider ||= build(:provider)
  end
end
