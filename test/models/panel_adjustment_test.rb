require 'test_helper'

class PanelAdjustmentTest < ActiveSupport::TestCase
  setup do 
  	@attr_for_panel_adjustment = attributes_for(:panel_adjustment)
  	@panel_adjustment = PanelAdjustment.new
    @base_panel = create(:base_panel)
    @panel_adjustment_name = create(:panel_adjustment_name)
    @qualifiers = ["TEST_PANEL", "NO", "TEST", "TEST ONLY", "YES", "TEST IF DECLARED", "OTHER", "Do not test for", "EIA"]
  end

  test 'is valid with valid attributes' do
  	panel_adjustment_name = create(:panel_adjustment_name)
  	@panel_adjustment.panel_adjustment_name = panel_adjustment_name
  	@panel_adjustment.qualifier = "TEST_PANEL"
  	@panel_adjustment.base_panel = @base_panel
  	@panel_adjustment.save
  	assert @panel_adjustment.errors.blank?
  end
  def show_error(object,attributes)
    begin
  	  object.send(:update, attributes)
    rescue ArgumentError => e
  	  return e.message
    end
  end

  test "qualifier only takes configured values" do
  	@attr_for_panel_adjustment[:qualifier] = "invalid_qualifier"
  	error_message = show_error(@panel_adjustment, @attr_for_panel_adjustment)
	  assert_equal "'invalid_qualifier' is not a valid qualifier", error_message
  end

  test "is not valid without test panel name" do
    @attr_for_panel_adjustment = @attr_for_panel_adjustment.merge(:panel_adjustment_name => nil, base_panel_id: @base_panel.id)
    @panel_adjustment.save(@attr_for_panel_adjustment)
    assert @panel_adjustment.errors[:panel_adjustment_name].present?
  end

  test "is not valid without base panel" do
    @attr_for_panel_adjustment = @attr_for_panel_adjustment.merge(:panel_adjustment_name => @panel_adjustment_name)
    @panel_adjustment.save(@attr_for_panel_adjustment)
    # assert @panel_adjustment.errors[:base_panel].present?
  end

end
