require 'test_helper'

class AddressTest < ActiveSupport::TestCase

  test "doesn't accept empty address line 1" do
    assert_equal ["can't be blank"], errors_when(address, :address_line_1, '')
  end

  test "doesn't accept empty city" do
    assert_equal ["can't be blank"], errors_when(address, :city, '')
  end

  test "doesn't accept empty state" do
    assert_equal ["can't be blank"], errors_when(address, :state, '')
  end

  test "doesn't accept empty ZIP code" do
    assert_equal ["can't be blank", "Incorrect format on ZIP code. ex) 70077 or 70077-7777"], errors_when(address, :zip, '')
  end

  test "doesn't accept incorrectly formatted ZIP code" do
    error_message = ["Incorrect format on ZIP code. ex) 70077 or 70077-7777"] 
    assert_equal error_message, errors_when(address, :zip, "ZIPCODE")
    assert_equal error_message, errors_when(address, :zip, "123456")
    assert_equal error_message, errors_when(address, :zip, "1234567")
    assert_equal error_message, errors_when(address, :zip, "Z1234567")
    assert_equal error_message, errors_when(address, :zip, "77ABCD-12")
  end

 private
 def address
 	@address ||= build(:address)
 end

end

