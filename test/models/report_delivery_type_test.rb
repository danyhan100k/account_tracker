require 'test_helper'

class ReportDeliveryTypeTest < ActiveSupport::TestCase
	test "has many AccountProviders" do
		account_report_delivery_type = create(:account_report_delivery_type)
		report_delivery_type.account_report_delivery_types << account_report_delivery_type
		assert_equal report_delivery_type, account_report_delivery_type.reload.report_delivery_type
	end

	test "has many Accounts through AccountProviders" do
		account = create(:account)
		account_report_delivery_type = create(:account_report_delivery_type, account: account)
		report_delivery_type.account_report_delivery_types << account_report_delivery_type
		assert_equal [account], report_delivery_type.accounts
	end

	private
		def report_delivery_type
			@report_delivery_type ||= create(:report_delivery_type)
		end
end
