require 'test_helper'

class SpecimanTest < ActiveSupport::TestCase

  test "doesn't accept empty batch value" do
    assert_equal ["can't be blank"], errors_when(specimen, :batch, '')
  end

  test "doesn't accept empty sample type vlaue" do
    assert_equal ["can't be blank"], errors_when(specimen, :sample_type_id, '')
  end

  test "doesn't accept empty date received value" do
    assert_equal ["can't be blank"], errors_when(specimen, :date_received, nil)
  end

   test "doesn't accept empty account_id" do
    assert_equal ["can't be blank"], errors_when(specimen, :account, nil)
  end

  test "doesn't accept empty requisition number value" do
    assert_equal ["can't be blank"], errors_when(specimen, :requisition_number, '')
  end

  test "doesn't accept empty is processable boolean value" do
    assert_equal ["can't be blank"], errors_when(specimen, :is_processable, nil)
  end

  test "doesn't accept empty is processable eia boolean value" do
    assert_equal ["can't be blank"], errors_when(specimen, :is_processable_eia, nil)
  end

  test "doesn't accept empty is_wet value" do 
    assert_equal ["can't be blank"], errors_when(specimen, :is_wet, nil)
  end

  test "doesn't accept empty is_stat value" do 
    assert_equal ["can't be blank"], errors_when(specimen, :is_stat, nil)
  end

  test "belongs to account" do 
    account = create(:account)
    specimen.account = account
    specimen.save!
    assert_equal account.id, specimen.account_id
  end

  test "find for eia report with the same date" do
    date = Date.yesterday
    specimen = create(
      :specimen,
      date_received: date,
      account: create(
        :account,
        base_panels: [
          create(:base_panel, panel_adjustments: [ create(:panel_adjustment) ])
        ]
      )
    )
    assert_equal [specimen], Specimen.find_for_eia_report(date_received: date)
  end

  test "find for eia report without NO EIA in the base panel name" do
    specimen = create(
      :specimen,
      date_received: set_date,
      account: create(
        :account,
        base_panels: [
          create(
            :base_panel,
            panel_adjustments: [
              create(:panel_adjustment, 
                      panel_adjustment_name: create(:panel_adjustment_name))
            ],
            base_panel_name: create(
              :base_panel_name,
              name: "Something NO EIA something else"
            )
          )
        ]
      )
    )
    assert_equal [], Specimen.find_for_eia_report(date_received: set_date)
  end

  test "find for eia report with processable as true" do 
    specimen = create(:specimen, 
                      is_processable: true, 
                      date_received: set_date,
                      account: create(:account, 
                                       base_panels: [
                                        create(:base_panel,
                                                panel_adjustments: [
                                                  create(:panel_adjustment, 
                                                          panel_adjustment_name: create(:panel_adjustment_name))
                                                ])
                                        ])
                      )
    assert_equal [specimen], Specimen.find_for_eia_report(date_received: specimen.date_received)
  end

  test "find for eia report with is_processable_eia as true" do
    specimen = create(:specimen, 
                      is_processable_eia: true, 
                      date_received: set_date,
                      account: create(:account, 
                                      base_panels: [ 
                                        create(:base_panel, 
                                                panel_adjustments: [ 
                                                  create(:panel_adjustment,
                                                          panel_adjustment_name: create(:panel_adjustment_name)
                                                          ) 
                                                ]) 
                                        ])
                      )
    assert_equal [specimen], Specimen.find_for_eia_report(date_received: specimen.date_received)
  end

  test "finding for eia report should not return specimen with panel adjustment name of 'EIA' with a qualifier of 'NO' " do 
    specimen = create(:specimen, date_received: set_date, 
                      account: create(:account, 
                                      base_panels: [ 
                                        create(:base_panel, 
                                                panel_adjustments: [
                                                  create(:panel_adjustment, 
                                                        panel_adjustment_name: create(:panel_adjustment_name,
                                                                                      name: "EIA"),
                                                        qualifier: 1
                                                         )
                                                ]) 
                                      ]
                                      ))
    assert_equal [], Specimen.find_for_eia_report(date_received: specimen.date_received)
  end

  test "finding for eia report returns specimen with panel adjustment name of 'EIA' with qualifier of 'YES'" do
    specimen = create(:specimen, date_received: set_date, 
                      account: create(:account, 
                                      base_panels: [ 
                                        create(:base_panel, 
                                                panel_adjustments: [
                                                  create(:panel_adjustment, 
                                                        panel_adjustment_name: create(:panel_adjustment_name,
                                                                                      name: "EIA"),
                                                        qualifier: 2
                                                         )
                                                ]) 
                                      ]
                                      ))
    assert_equal [specimen], Specimen.find_for_eia_report(date_received: specimen.date_received)
  end
  test "finding for eia report returns specimen with panel adjustment name of 'anything' with qualifier of 'NO'" do
   specimen = create(:specimen, date_received: set_date, 
                      account: create(:account, 
                                      base_panels: [ 
                                        create(:base_panel, 
                                                panel_adjustments: [
                                                  create(:panel_adjustment, 
                                                        panel_adjustment_name: create(:panel_adjustment_name,
                                                                                      name: "ANYTHING"),
                                                        qualifier: 2
                                                         )
                                                ]) 
                                      ]
                                      )) 
   assert_equal [specimen], Specimen.find_for_eia_report(date_received: specimen.date_received)
  end

  test "finding for eia report does not return specimen with qualifier of 'NO' and panel adjustment name of 'EIA' 
  even if it has other panel adjustments that pass the criteria" do 
    account = create(:account, 
                      base_panels: [
                        create(:base_panel, 
                              panel_adjustments: [
                                create(:panel_adjustment,
                                       panel_adjustment_name: create(:panel_adjustment_name, name: 'EIA'),
                                       qualifier: 1),
                                create(:panel_adjustment, 
                                       panel_adjustment_name: create(:panel_adjustment_name, name: 'PASSES'),
                                       qualifier: 2)
                              ])
                      ])
    specimen = create(:specimen, account: account, date_received: set_date)

    assert_equal [], Specimen.find_for_eia_report(date_received: specimen.date_received)

  end


  private
  def specimen
  	@specimen ||= build(:specimen)
  end
  
  def set_date
    @date ||= Date.yesterday
  end
end
