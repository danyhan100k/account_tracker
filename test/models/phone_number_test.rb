require 'test_helper'

class PhoneNumberTest < ActiveSupport::TestCase
	test "it accepts valid phone numbers" do
		assert_equal [], errors_when(phone_number, :number, "777-777-7777")
	end

	test "it doesn't accept invalid phone numbers" do
		# assert_equal ["can't be blank", "has invalid Format. ex) 847-777-7777"], errors_when(phone_number, :number, "")
		# assert_equal ["has invalid Format. ex) 847-777-7777"], errors_when(phone_number, :number, "777 777 7777")
		# assert_equal ["has invalid Format. ex) 847-777-7777"], errors_when(phone_number, :number, "777-777-77778")
		# assert_equal ["has invalid Format. ex) 847-777-7777"], errors_when(phone_number, :number, "(777) 777-7777")
		# assert_equal ["has invalid Format. ex) 847-777-7777"], errors_when(phone_number, :number, "HI")
	end

	test "it accepts notes smaller than 256 characters" do
		assert_equal [], errors_when(phone_number, :notes, "a" * 255)
	end

	test "it doesn't accept notes longer than 255 characters" do
		assert_equal ["255 characters is the maximum allowed"], errors_when(phone_number, :notes, "a" * 256)
	end

	private
	def phone_number
		@phone_number ||= PhoneNumber.new
	end
end
