require 'test_helper'

class AccountProviderTest < ActiveSupport::TestCase
   test "does not accept empty account" do 
   	account_provider = new_account_provider
   	assert_equal ["can't be blank"], errors_when(account_provider, :account, nil)
   end

	 test "does not accept empty provider" do 
	 	account_provider = new_account_provider
	 	assert_equal ["can't be blank"], errors_when(account_provider, :provider, nil)
	 end

	 test "belongs to account" do 
	 	account = create(:account)
	 	account_provider = new_account_provider
	 	account_provider.assign_attributes(account: account)
	 	account_provider.save!
	 	assert_equal account.id, account_provider.account_id
	 end

	 test "belongs to provider" do 
	 	provider = create(:provider)
	 	account_provider = new_account_provider
	 	account_provider.assign_attributes(provider: provider)
	 	account_provider.save!
	 	assert_equal provider.id, account_provider.provider_id
	 end

private
	def new_account_provider
		@account_provider ||= build(:account_provider)
	end
end
