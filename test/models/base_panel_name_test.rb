require 'test_helper'

class BasePanelNameTest < ActiveSupport::TestCase
 	
 	test "doesn't accept empty name" do
    assert_equal ["can't be blank"], errors_when(new_base_panel_name, :name, nil)
  end

  test "accepts name longer than 255" do
    new_base_panel_name.name = "a" * 256
    new_base_panel_name.save!
  end

  test "has many BasePanels" do
    new_base_panel_name.save!
    base_panel = create(:base_panel)
    new_base_panel_name.base_panels << base_panel
    assert_equal [base_panel], new_base_panel_name.base_panels
  end

  private
  def new_base_panel_name
  	@base_panel_name ||= build(:base_panel_name)
  end

end
