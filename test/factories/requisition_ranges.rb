FactoryGirl.define do
  factory :requisition_range do
    starts_at 1
    ends_at 1
    account
  end
end
