FactoryGirl.define do
  factory :specimen do
  	date_released "1970-09-24 00:00:00"
  	date_processed "1970-09-24 00:00:00"
    date_received "1970-09-24"
  	is_processable true
    is_processable_eia true
    batch 1
    requisition_number "123"
    sample_type_id 1
    is_wet true
    is_stat true
    
  	association :account
  end
end
