FactoryGirl.define do
  factory :address do
  	address_line_1 "777 n street ave"
  	address_line_2 "apt B"
  	city "vernon hills"
  	state "IL"
  	zip "60012"
  	notes "For delivery only"
  end
end


  