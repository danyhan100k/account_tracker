FactoryGirl.define do
  factory :provider do
    name "PROVIDER"
    npi_number "99999"
  end
end
