FactoryGirl.define do
  factory :account_report_delivery_type do
    account
    report_delivery_type
  end
end
