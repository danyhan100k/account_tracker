FactoryGirl.define do
  factory :panel_adjustment do
  	qualifier "TEST_PANEL"
  	association :panel_adjustment_name
  	association :base_panel
  end
end
