FactoryGirl.define do
  factory :account_number do
  	sequence(:number) { |n| n.to_s.rjust(6, '0') }
  	notes "long term customer"
  	association :account

  	factory :account_number_with_one_address do
  		after(:create) do |account_number|
  			create(:address, addressable: account_number )
  		end
  	end
  	factory :account_number_with_many_addresses do
  		after(:create) do |account_number|
	  		2.times { create(:address, addressable: account_number ) }
  		end
  	end
  end
end
