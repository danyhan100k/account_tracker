FactoryGirl.define do
  factory :result_set do
    result_set_code "MyString"
    description "MyString"
    drug_class "MyString"
    billing_class "MyString"
    billing_code "MyString"
  end
end
