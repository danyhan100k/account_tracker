FactoryGirl.define do
  factory :account do
    sequence(:prefix) { |i| "A#{i}" }
    company_name  "Great company"
    notes "our customer for a long time"
    has_sample_collector "true"
    association :sales_rep, manager_title: "manager"
  end
end
