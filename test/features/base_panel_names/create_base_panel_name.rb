require 'test_helper'


class BasePanelNamesCreateBasePanelNameTest < ActionDispatch::IntegrationTest
	test "the user should be able to navigate to the new base panel name page" do
		visit '/admin'
		within('.sidebar-nav') do 
			click_on 'Base panel names'
		end
		click_on 'Add new'
		assert page.has_content?('New Base panel name')
		assert_equal '/admin/base_panel_name/new', current_path
	end
	test "the user should be able to create new base panel name" do
		visit 'admin/base_panel_name/new'
		fill_in('Name', with: 'Customized pain management panel')
		click_on "Save"
		assert page.has_content?("Base panel name successfully created")
		assert page.has_content?("Customized pain management panel")
	end
end
