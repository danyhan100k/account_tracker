require 'test_helper'


class ProvidersCreateProvidersTest < ActionDispatch::IntegrationTest
	test "the user should be able to navigate to new provider page" do
		visit '/admin'
		within('.sidebar-nav') do 
			click_on 'Providers'
		end
		click_on 'Add new'
		assert page.has_content?('New Provider')
		assert_equal '/admin/provider/new', current_path
	end
	test "the user should be able to create new provider" do
		visit 'admin/provider/new'
		fill_in('Name', with: 'Dr.Phil')
		fill_in('Npi number', with: 7777777)
		click_on "Save"
		assert page.has_content?("Provider successfully created")
		assert page.has_content?("Dr.Phil")
	end
end