require 'test_helper'

class TestPanelsCreateTestPanels < ActionDispatch::IntegrationTest
  test "the user should be able to navigate to new Test Panel page" do 
    visit '/admin'
    within('.sidebar-nav') do
      find_link('Panel adjustments').click
    end
    click_on 'Add new'
    assert page.has_content?('Panels adjustments')
    assert_equal "/admin/panel_adjustment_name/new", current_path
  end

  def select_qualifier(name)
    find('#panel_adjustment_qualifier_field .dropdown-toggle').trigger('click')
    find(".ui-menu-item a", text: name).click
  end

  def select_panel_adjustment_name(name)
    find('#panel_adjustment_panel_adjustment_name_id_field .dropdown-toggle').click
    find(".ui-menu-item a", text: name).click
  end

  test "the user should be able to create new Test Panel" do 
    panel_adjustment_name = create(:panel_adjustment_name, name: 'gabapentin')
    visit "/admin/panel_adjustment_name/new"
    select_panel_adjustment_name(panel_adjustment_name.name)
    select_qualifier("TEST_ONLY")
    click_on "Save"
    assert_equal "/admin/panel_adjustments", current_path
  end

end
