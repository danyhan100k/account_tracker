require 'test_helper'

class ReportDeliveryTypesCreateReportDeliveryTypeTest < ActionDispatch::IntegrationTest
	test "the user should be able to navigate to new report delivery type page" do
		visit '/admin'
		within('.sidebar-nav') do 
			click_on 'Report delivery types'
		end
		click_on 'Add new'
		assert page.has_content?('New Report delivery type')
		assert_equal '/admin/report_delivery_type/new', current_path
	end
	test "the user should be able to create new report delivery type" do
		visit 'admin/report_delivery_type/new'
		fill_in('Name', with: 'fax')
		click_on "Save"
		assert page.has_content?("Report delivery type successfully created")
		assert page.has_content?("fax")
	end
end