require 'test_helper'

class AccountsCreateAccountTest < ActionDispatch::IntegrationTest
  test "the user should be able to navigate to the new account page" do
    visit "/admin"
    within('.sidebar-nav') do
      click_on 'Accounts'
    end
    click_on 'Add new'
    assert page.has_content?('New Account')
    assert_equal "/admin/account/new", current_path
  end

  test "the user should be able to create a new account" do
    sales_rep = create(:sales_rep, manager_title: "manager")
    provider = create(:provider)
    visit "/admin/account/new"
    fill_in 'Prefix', with: 'A5'
    fill_in 'Company name', with: 'ACME'
    fill_in 'Notes', with: 'Custom notes'

    select_from_autocomplete(:account, :sales_rep, sales_rep.name)
    select_from_multiselect('#account_provider_ids_field', provider.name)

    check 'Has sample collector'
    click_on 'Save'

    assert page.has_content?("Account successfully created")
    assert page.has_content?("A5")
    assert page.has_content?("ACME")
  end
end
