require 'test_helper'

class SpecimensCreateSpecimen < ActionDispatch::IntegrationTest
	test "the user should be able to navigate to the new specimen page" do
		visit '/admin'
		within('.sidebar-nav') do
			click_on 'Specimens'
		end
		click_on 'Add new'
		assert page.has_content?('New Specimen')
		assert_equal "/admin/specimen/new", current_path
	end
	
	test "the user should be able to create new specimen" do
		account = create(:account)
		visit '/admin/specimen/new'
		fill_specimen_form(specimen_attributes,account)
		click_on("Save")
		assert_equal "/admin/specimen", current_path
		assert page.has_content?(specimen_attributes[:sample_type])
		assert page.has_content?(specimen_attributes[:sample_name])
	end

	private
	def fill_specimen_form(specimen_attributes,account)
		fill_in('Sample name', with: specimen_attributes[:sample_name])
		fill_in('Sample type', with: specimen_attributes[:sample_type])
		fill_in('Accession number', with: specimen_attributes[:accession_number])
		check('Processable')
		find("#specimen_account_id_field .dropdown-toggle").click
    find(".ui-menu-item a", text: account.company_name).click
		select_date(:received_at, Date.today)
		select_date(:date_released, Date.tomorrow)
	end

	def select_date(attribute, date)
		find("#specimen_#{attribute}").click
		find("[data-day=\"#{date.strftime('%m/%d/%Y')}\"]").click
	end

	def specimen_attributes
		@specimen_attributes ||= attributes_for(:specimen)
	end
end
