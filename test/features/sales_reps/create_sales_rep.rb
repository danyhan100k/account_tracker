require 'test_helper'
class SalesRepsCreateSalesRepTest < ActionDispatch::IntegrationTest
	test "the user should be able to navigate to new Sales Rep page" do 
		visit '/admin'
		within('.sidebar-nav') do
			find_link('Sales reps').click
		end
		click_on 'Add new'
    assert page.has_content?('New Sales rep')
    assert_equal "/admin/sales_rep/new", current_path
	end
	test "the user should be able to create new Sales Rep as manager" do
        visit "/admin/sales_rep/new"
        fill_sales_rep(manager_attributes)
        add_address
        check('Is manager')
        fill_in('Manager title', with: "Supreme Manager")
        click_on("Save")
        assert_equal "/admin/sales_rep", current_path
        assert page.has_content?(manager_attributes[:first_name])
        assert page.has_content?(manager_attributes[:last_name])
	end 
	test "the user should be able to create new Sales Rep as non-manager, with manager specified" do
        manager = create(:sales_rep,first_name: "Mr. Manager", manager_title: "house manager")
        visit "/admin/sales_rep/new"
        fill_sales_rep(attributes_for(:sales_rep))
        add_address
        # uncheck('Is manager')
        select_from_autocomplete(:sales_rep, :manager, manager.first_name)
        click_on("Save")
        assert_equal "/admin/sales_rep", current_path
        assert page.has_content?(attributes_for(:sales_rep)[:first_name])
        assert page.has_content?(attributes_for(:sales_rep)[:last_name])
	end 
  test "the user should be able to create new independent Sales Rep as non-manager, without manager association" do
      visit "/admin/sales_rep/new"
      fill_sales_rep(attributes_for(:sales_rep))
      add_address
      uncheck('Is manager')
      click_on("Save")
      assert_equal "/admin/sales_rep", current_path
      assert page.has_content?(attributes_for(:sales_rep)[:first_name])
      assert page.has_content?(attributes_for(:sales_rep)[:last_name])
  end 

  private
  def manager_attributes
    @manager_attributes ||= attributes_for(:sales_rep)
  end

  def fill_sales_rep(source)
		fill_in('First name', with: source[:first_name])
		fill_in('Last name', with: source[:last_name])
		fill_in('Email', with: source[:email])
  end
  
  def add_address
    click_on("Add a new Address")
    fill_in('Address line 1', with: "Some address")
    fill_in('Address line 2', with: "apartment 33")
    fill_in('City', with: "something")
    fill_in('State', with: "something")
    fill_in('Zip', with: "77777")
  end

  def add_phone_number
    click_on("Add a new Phone number")
    fill_in('Number', with: "777-777-7777")
    fill_in('Number type', with: "something")
  end
end
