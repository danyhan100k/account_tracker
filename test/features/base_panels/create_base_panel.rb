require 'test_helper'


class BasePanelsCreateBasePanel < ActionDispatch::IntegrationTest
	test "the user should be able to navigate to the new base panel page" do 
		visit '/admin'
		within('.sidebar-nav') do 
			click_on 'Base panels'
		end 
		click_on 'Add new'
		assert page.has_content?('New Base panel')
		assert_equal "/admin/base_panel/new", current_path
	end
	def fill_base_panel
    find('#base_panel_base_panel_name_id_field .dropdown-toggle').trigger('click')
    find(".ui-menu-item a", text: name).trigger('click')
	end
	def fill_panel_adjustment_name(name)
		find('.form-group control-group belongs_to_association_type panel_adjustment_name_field  .dropdown-toggle').click
		find(".ui-menu-item a", text: name).trigger('click')
	end

	def select_qualifier(name)
    find('.form-group control-group enum_type qualifier_field').trigger('click')
    find(".ui-menu-item a", text: name).click
  end

	test "the user should be able to create new base panels" do
		visit 'admin/base_panel/new' 
		base_panel_name = create(:base_panel_name, name: "Customized Pain Management Panel")		
		panel_adjustment_name = create(:panel_adjustment_name, name: "Benzodiazepines")
		fill_base_panel(base_panel_name.name)
		click_on "Add a new Panel adjustment"
		fill_panel_adjustment_name(panel_adjustment_name_name.name)
		select_qualifier("YES")
		click_on 'Save'
		assert_equal '/admin/base_panel', current_path
		assert page.has_content?(base_panel_name.name)
	end
end
