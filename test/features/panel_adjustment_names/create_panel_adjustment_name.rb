require 'test_helper'

class PanelAdjustmentNamesCreatePanelAdjustmentNameTest < ActionDispatch::IntegrationTest
  test "the user should be able to navigate to new Test Panel Name page" do 
    visit '/admin'
    within('.sidebar-nav') do
      find_link('Panel adjustment names').click
    end
    click_on 'Add new'
    assert page.has_content?('New Panel adjustment name')
    assert_equal "/admin/panel_adjustment_name/new", current_path
  end

  test "the user should be able to create new Test Panel Name" do 
    name_for_panel_adjustment = attributes_for(:panel_adjustment_name)['name']
    visit "/admin/panel_adjustment_name/new"
    fill_in("Name", with: name_for_panel_adjustment)
    click_on "Save"
    assert page.has_content?(name_for_panel_adjustment)
    assert_equal "/admin/panel_adjustment_name", current_path
  end
end
