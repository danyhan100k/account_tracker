# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

report_delivery_type = ["Fax", "Online Client Login"]
n_of_records = 50

1.upto(n_of_records) do |n|
	SalesRep.create!({
		first_name: "Mr. #{n}",
		email: "sales_#{n}@gmail.com"
	})

	Account.create!({
		prefix: "#{('A'..'Z').to_a.sample}#{n}",
		company_name: "Company_#{n}",
		sales_rep_id: n,
		has_sample_collector: [true, false].sample
	})

	AccountNumber.create!({
		account_id: n,
		number: n.to_s * 7
	})
	PhoneNumber.create!({ phoneable_id: n, phoneable_type: 'SalesRep', number: "555-555-5555", number_type: ["fax", "cell", "office"].sample })
	PhoneNumber.create!({ phoneable_id: n, phoneable_type: 'AccountNumber', number: "777-777-7777", number_type: ["fax", "cell", "office"].sample })
  Address.create!({ addressable_id: n, addressable_type: "AccountNumber", address_line_1: "#{n.to_s * 3} west ave", city: "Reno", state: "Neveda", zip: "77777"})

  Provider.create!({ name: "Provider_#{n}", npi_number: "#{n.to_s * 12 }"})
	AccountProvider.create!({ account_id: n, provider_id: n })

	BasePanelName.create!({ name: "#{n} Drug Panel"})
	BasePanel.create!({ base_panel_name_id: n, account_id: n})
	PanelAdjustmentName.create!({ name: ["Benzodiazepines", "Meperidine", "Gabapentin", "Pregabalin"].sample, })
	PanelAdjustment.create!({ panel_adjustment_name_id: n, base_panel_id: n, qualifier: ["TEST_PANEL", "NO", "TEST"].sample})

	if report_delivery_type.present?
		ReportDeliveryType.create!(name: "#{report_delivery_type.pop}")
	end
	AccountReportDeliveryType.create!(account_id: n, report_delivery_type_id: ReportDeliveryType.all.sample.id)

end







