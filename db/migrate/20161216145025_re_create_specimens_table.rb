class ReCreateSpecimensTable < ActiveRecord::Migration
  def up
  	create_table :specimens do |t|
      t.date :date_received
      t.integer :batch, null: false, default: 1
      t.belongs_to :account, index: true, foreign_key: true
      t.string :requisition_number, null: false, unique: true
      t.boolean :is_processable, null: false, default: true
      t.boolean :is_processable_eia, null: false, default: true
      t.boolean :is_wet, null: false, default: false
      t.boolean :is_stat, null: false, default: false
      t.text :notes_lab
      t.text :notes_clerical
      t.text :notes_sales_rep
      t.text :notes_other
      t.integer :sample_type_id, null: false
      t.string :insurance_name
      t.string :provider_name
      t.integer :age
      t.datetime :date_processed
      t.datetime :date_released
      t.string :created_by
      t.timestamps null: false
    end
  end
  def down 
  	drop_table :specimens
  end
end
