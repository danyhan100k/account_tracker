class CreateResultSets < ActiveRecord::Migration
  def change
    create_table :result_sets do |t|
      t.string :result_set_code
      t.string :description
      t.string :drug_class
      t.string :billing_class
      t.string :billing_code

      t.timestamps null: false
    end
  end
end
