class RenameTestPanelNamesToPanelAdjustmentNames < ActiveRecord::Migration
  def change
    rename_table :test_panel_names, :panel_adjustment_names
  end
end
