class ChangeTypeOfNameToTextFromPanelAdjusmentNames < ActiveRecord::Migration
  def up
    change_column :panel_adjustment_names, :name, :text
  end

  def down
    change_column :panel_adjustment_names, :name, :string
  end
end
