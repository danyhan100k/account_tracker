class CreateOrderSets < ActiveRecord::Migration
  def change
    create_table :order_sets do |t|
      t.string :order_set_code
      t.string :description

      t.timestamps null: false
    end
  end
end
