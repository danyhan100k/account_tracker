class AddAuthTokenToSalesReps < ActiveRecord::Migration
  def up
  	add_column :sales_reps, :auth_token, :string
  end
  def down
  	remove_column :sales_reps, :auth_token, :string
  end
end
