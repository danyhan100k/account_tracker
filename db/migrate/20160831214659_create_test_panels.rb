class CreateTestPanels < ActiveRecord::Migration
  def change
    create_table :test_panels do |t|
      t.belongs_to :test_panel_name, index: true, foreign_key: true
      t.belongs_to :base_panel, index: true, foreign_key: true
      t.integer :qualifier

      t.timestamps null: false
    end
  end
end
