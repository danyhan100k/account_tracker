class RenameTestPanelNameIdToPanelAdjustmentNameIdFromPanelAdjustments < ActiveRecord::Migration
  def change
    rename_column :panel_adjustments, :test_panel_name_id, :panel_adjustment_name_id
  end
end
