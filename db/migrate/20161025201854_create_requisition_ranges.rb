class CreateRequisitionRanges < ActiveRecord::Migration
  def change
    create_table :requisition_ranges do |t|
      t.integer :starts_at
      t.integer :ends_at
      t.belongs_to :account, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
