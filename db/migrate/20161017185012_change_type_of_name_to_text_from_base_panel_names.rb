class ChangeTypeOfNameToTextFromBasePanelNames < ActiveRecord::Migration
  def up
    change_column :base_panel_names, :name, :text
  end

  def down
    change_column :base_panel_names, :name, :string
  end
end
