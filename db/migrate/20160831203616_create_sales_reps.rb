class CreateSalesReps < ActiveRecord::Migration
  def change
    create_table :sales_reps do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.belongs_to :manager, index: true
      t.string :manager_title

      t.timestamps null: false
    end
  end
end
