class DropSpecimensTable < ActiveRecord::Migration
  def up
  	drop_table :specimens
  end

  def down
  	create_table :specimens do |t|
      t.string :sample_name
      t.string :sample_type
      t.date :received_at  
      t.belongs_to :account, index: true, foreign_key: true
      t.text :accession_number
      t.boolean :processable, default: true
      t.datetime :date_released

      t.timestamps null: false
    end
  end
end
