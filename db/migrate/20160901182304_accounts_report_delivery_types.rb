class AccountsReportDeliveryTypes < ActiveRecord::Migration
  def change
  	create_table :accounts_report_delivery_types, id: false do |t|
  		t.integer :account_id, foreign_key: true
  		t.integer :report_delivery_type_id, foreign_key: true
  	end
  	add_index :accounts_report_delivery_types, :account_id, name: 'index_on_report_deliverty_type_on_rd_and_accounts_join_t'
  	add_index :accounts_report_delivery_types, :report_delivery_type_id, name: 'index_on_account_id_on_account_delivery_t_join_t'
  end
end
