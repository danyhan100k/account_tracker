class CreateAccountProviders < ActiveRecord::Migration
  def change
    create_table :account_providers do |t|
      t.belongs_to :account, index: true, foreign_key: true
      t.belongs_to :provider, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
