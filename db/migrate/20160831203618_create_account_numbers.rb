class CreateAccountNumbers < ActiveRecord::Migration
  def change
    create_table :account_numbers do |t|
      t.belongs_to :account, index: true, foreign_key: true
      t.string :number
      t.string :notes

      t.timestamps null: false
    end
  end
end
