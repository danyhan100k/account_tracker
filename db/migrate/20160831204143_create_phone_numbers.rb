class CreatePhoneNumbers < ActiveRecord::Migration
  def change
    create_table :phone_numbers do |t|
      t.belongs_to :phoneable, polymorphic: true, index: true
      t.string :number
      t.string :number_type
      t.string :notes

      t.timestamps null: false
    end
  end
end
