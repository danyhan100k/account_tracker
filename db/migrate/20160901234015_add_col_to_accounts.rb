class AddColToAccounts < ActiveRecord::Migration
  def change
    add_column :accounts, :has_sample_collector, :boolean, default: false
  end
end
