class CreateBasePanels < ActiveRecord::Migration
  def change
    create_table :base_panels do |t|
      t.belongs_to :base_panel_name, index: true, foreign_key: true
      t.belongs_to :account, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
