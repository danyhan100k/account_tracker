class CreateAccountNumbersOrderSets < ActiveRecord::Migration
  def change
    create_table :account_numbers_order_sets do |t|
      t.integer :account_number_id
      t.integer :order_set_id

      t.timestamps null: false
    end
  end
end
