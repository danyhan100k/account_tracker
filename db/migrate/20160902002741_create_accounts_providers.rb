class CreateAccountsProviders < ActiveRecord::Migration
  def change
    create_table :accounts_providers, id: false do |t|
      t.belongs_to :account, index: true, foreign_key: true
      t.references :provider, index: true, foreign_key: true
    end
  end
end
