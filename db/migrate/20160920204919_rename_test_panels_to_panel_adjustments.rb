class RenameTestPanelsToPanelAdjustments < ActiveRecord::Migration
  def change
    rename_table :test_panels, :panel_adjustments
  end
end
