class CreateOrderSetsResultSets < ActiveRecord::Migration
  def change
    create_table :order_sets_result_sets do |t|
      t.integer :order_set_id
      t.integer :result_set_id

      t.timestamps null: false
    end
  end
end
