class CreateBasePanelNames < ActiveRecord::Migration
  def change
    create_table :base_panel_names do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
