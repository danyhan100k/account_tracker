# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161216145025) do

  create_table "account_numbers", force: :cascade do |t|
    t.integer  "account_id", limit: 4
    t.string   "number",     limit: 255
    t.string   "notes",      limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "account_numbers", ["account_id"], name: "index_account_numbers_on_account_id", using: :btree

  create_table "account_numbers_order_sets", force: :cascade do |t|
    t.integer  "account_number_id", limit: 4
    t.integer  "order_set_id",      limit: 4
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "account_providers", force: :cascade do |t|
    t.integer  "account_id",  limit: 4
    t.integer  "provider_id", limit: 4
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "account_providers", ["account_id"], name: "index_account_providers_on_account_id", using: :btree
  add_index "account_providers", ["provider_id"], name: "index_account_providers_on_provider_id", using: :btree

  create_table "account_report_delivery_types", force: :cascade do |t|
    t.integer  "account_id",              limit: 4
    t.integer  "report_delivery_type_id", limit: 4
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  add_index "account_report_delivery_types", ["account_id"], name: "index_account_report_delivery_types_on_account_id", using: :btree
  add_index "account_report_delivery_types", ["report_delivery_type_id"], name: "index_account_report_delivery_types_on_report_delivery_type_id", using: :btree

  create_table "accounts", force: :cascade do |t|
    t.string   "prefix",               limit: 255
    t.string   "company_name",         limit: 255
    t.string   "notes",                limit: 255
    t.integer  "sales_rep_id",         limit: 4
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
    t.boolean  "has_sample_collector",             default: false
  end

  add_index "accounts", ["sales_rep_id"], name: "index_accounts_on_sales_rep_id", using: :btree

  create_table "addresses", force: :cascade do |t|
    t.integer  "addressable_id",   limit: 4
    t.string   "addressable_type", limit: 255
    t.string   "address_line_1",   limit: 255
    t.string   "address_line_2",   limit: 255
    t.string   "city",             limit: 255
    t.string   "state",            limit: 255
    t.string   "zip",              limit: 255
    t.string   "notes",            limit: 255
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "addresses", ["addressable_type", "addressable_id"], name: "index_addresses_on_addressable_type_and_addressable_id", using: :btree

  create_table "base_panel_names", force: :cascade do |t|
    t.text     "name",       limit: 65535
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "base_panels", force: :cascade do |t|
    t.integer  "base_panel_name_id", limit: 4
    t.integer  "account_id",         limit: 4
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "base_panels", ["account_id"], name: "index_base_panels_on_account_id", using: :btree
  add_index "base_panels", ["base_panel_name_id"], name: "index_base_panels_on_base_panel_name_id", using: :btree

  create_table "order_sets", force: :cascade do |t|
    t.string   "order_set_code", limit: 255
    t.string   "description",    limit: 255
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "order_sets_result_sets", force: :cascade do |t|
    t.integer  "order_set_id",  limit: 4
    t.integer  "result_set_id", limit: 4
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "panel_adjustment_names", force: :cascade do |t|
    t.text     "name",       limit: 65535
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "panel_adjustments", force: :cascade do |t|
    t.integer  "panel_adjustment_name_id", limit: 4
    t.integer  "base_panel_id",            limit: 4
    t.integer  "qualifier",                limit: 4
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
  end

  add_index "panel_adjustments", ["base_panel_id"], name: "index_panel_adjustments_on_base_panel_id", using: :btree
  add_index "panel_adjustments", ["panel_adjustment_name_id"], name: "index_panel_adjustments_on_panel_adjustment_name_id", using: :btree

  create_table "phone_numbers", force: :cascade do |t|
    t.integer  "phoneable_id",   limit: 4
    t.string   "phoneable_type", limit: 255
    t.string   "number",         limit: 255
    t.string   "number_type",    limit: 255
    t.string   "notes",          limit: 255
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "phone_numbers", ["phoneable_type", "phoneable_id"], name: "index_phone_numbers_on_phoneable_type_and_phoneable_id", using: :btree

  create_table "providers", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "npi_number", limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "report_delivery_types", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "requisition_ranges", force: :cascade do |t|
    t.integer  "starts_at",  limit: 4
    t.integer  "ends_at",    limit: 4
    t.integer  "account_id", limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "requisition_ranges", ["account_id"], name: "index_requisition_ranges_on_account_id", using: :btree

  create_table "result_sets", force: :cascade do |t|
    t.string   "result_set_code", limit: 255
    t.string   "description",     limit: 255
    t.string   "drug_class",      limit: 255
    t.string   "billing_class",   limit: 255
    t.string   "billing_code",    limit: 255
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "sales_reps", force: :cascade do |t|
    t.string   "first_name",    limit: 255
    t.string   "last_name",     limit: 255
    t.string   "email",         limit: 255
    t.integer  "manager_id",    limit: 4
    t.string   "manager_title", limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "auth_token",    limit: 255
  end

  add_index "sales_reps", ["manager_id"], name: "index_sales_reps_on_manager_id", using: :btree

  create_table "specimens", force: :cascade do |t|
    t.date     "date_received"
    t.integer  "batch",              limit: 4,     default: 1,     null: false
    t.integer  "account_id",         limit: 4
    t.string   "requisition_number", limit: 255,                   null: false
    t.boolean  "is_processable",                   default: true,  null: false
    t.boolean  "is_processable_eia",               default: true,  null: false
    t.boolean  "is_wet",                           default: false, null: false
    t.boolean  "is_stat",                          default: false, null: false
    t.text     "notes_lab",          limit: 65535
    t.text     "notes_clerical",     limit: 65535
    t.text     "notes_sales_rep",    limit: 65535
    t.text     "notes_other",        limit: 65535
    t.integer  "sample_type_id",     limit: 4,                     null: false
    t.string   "insurance_name",     limit: 255
    t.string   "provider_name",      limit: 255
    t.integer  "age",                limit: 4
    t.datetime "date_processed"
    t.datetime "date_released"
    t.string   "created_by",         limit: 255
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
  end

  add_index "specimens", ["account_id"], name: "index_specimens_on_account_id", using: :btree

  add_foreign_key "account_numbers", "accounts"
  add_foreign_key "account_providers", "accounts"
  add_foreign_key "account_providers", "providers"
  add_foreign_key "account_report_delivery_types", "accounts"
  add_foreign_key "account_report_delivery_types", "report_delivery_types"
  add_foreign_key "accounts", "sales_reps"
  add_foreign_key "base_panels", "accounts"
  add_foreign_key "base_panels", "base_panel_names"
  add_foreign_key "panel_adjustments", "base_panels"
  add_foreign_key "panel_adjustments", "panel_adjustment_names"
  add_foreign_key "requisition_ranges", "accounts"
  add_foreign_key "specimens", "accounts"
end
