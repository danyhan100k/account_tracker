class SalesRepsController < ApplicationController
  def index
    respond_to do |format|
      format.json do
          render json: SalesRepSearchDatatablesAdapter.new(params)
      end
      format.html do 
        @sales_reps = SalesRep.all.order(first_name: :asc)
        render
      end
    end
  end

  def show 
  	begin
  		@sales_rep = SalesRep.find(params[:id])
  	rescue => e
  		flash[:alert] = "#{e.to_s}"
  		redirect_to sales_reps_path
  	end
  end
end
