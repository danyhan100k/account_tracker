class AccountsController < ApplicationController
  def index
    respond_to do |format|
      format.html do
        render
      end
      format.json do
        render json: AccountSearchDatatablesAdapter.new(params)
      end
    end
  end

  def accounts_search_table
    render partial: 'accounts/accounts_search_table'
  end

  def import
    begin
      spreadsheet = Roo::Spreadsheet.open(params[:file].path)
      sheet = spreadsheet.sheet(0)
      AccountsImporter.new.import sheet
    rescue AccountsImporter::ImportError => e   
      tempfile = Tempfile.open("import_errors") { |f| f << e.errors.to_json }
      return render json: { tempfile: tempfile.path, errors: e.errors }
    end
    redirect_to root_url, notice: "Accounts Imported."
  end
end
