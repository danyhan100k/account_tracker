class SpecimensController < ApplicationController

	def accession_log
	end
	
	def generate_report
		@specimens = Specimen.where(id: params[:id].split(','))
		respond_to do |format|
			format.csv do 
					filename = "EIA-client-report-#{Time.now.strftime("%Y/%m/%d")}.csv"
					send_data @specimens.to_csv(:only => [:account_prefix, :requisition_number]), 
										:filename => filename,
										:type => "text/csv; charset=utf-8; header=present"
			end
		end
	end

	def check_for_report
		respond_to do |format|
			format.json do
				date_received = Time.zone.parse(params['specimen_date_received']).to_date
				specimens = Specimen.find_for_eia_report(date_received: date_received)
				found_count = specimens.size
				if found_count.zero?
					render json: { found_count: found_count }
				else
					ids = specimens.map(&:id)
					render json: { found_count: found_count, id: ids }
				end		
			end
		end 
	end

	def pick_a_date
	end
end
