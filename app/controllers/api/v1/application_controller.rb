class API::V1::ApplicationController < ApplicationController
  skip_before_action :verify_authenticity_token
  rescue_from ActiveRecord::RecordInvalid, with: :treat_invalid_record_error

  private

    def treat_invalid_record_error(error)    	
      render json: { error: error.record.errors }, status: :unprocessable_entity
    end
end
