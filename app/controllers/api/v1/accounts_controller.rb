class API::V1::AccountsController < ApplicationController
  def index
    render json: Account.all
  end

	def show
		begin
			render json: Account.render_by_req_num(params[:id])
		rescue => e
			error_message = e.message
			if e.class == ActiveRecord::RecordNotFound
				error_message = "Could not find Account for requisition number #{params[:id]}"
			end
			render json: { :errors => error_message}, status: 422
		end
	end
end
