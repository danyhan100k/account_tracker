class API::V1::SpecimensController < API::V1::ApplicationController
  def index
    specimens = Specimen.where(date_received: params[:date_received])
    render json: specimens
  end

  def create
    specimen = Specimen.find_by(requisition_number: specimen_params[:requisition_number])
    if specimen
      return render json: { specimen: specimen }, status: :conflict
    end

    specimen = Specimen.new(specimen_params)
    specimen.account = Account.find_by(prefix: params[:specimen][:account_prefix])

    #if params[:selected_date_received].to_date != specimen.date_received
    #  return error
    #end

    if specimen.save!
      render json: specimen
    end
  end

  def update
    specimen = Specimen.find(params[:id])
    specimen.account_prefix = params['specimen']['account_prefix']
    if specimen.update!(specimen_params)
      render json: specimen
    end
  end

  private
    def specimen_params
      params.require(:specimen).permit(
        :batch,
        :created_by,
        :date_received,
        :is_processable,
        :requisition_number,
        :sample_type_id
      )
    end
end
