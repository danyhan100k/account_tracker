class AccountSearch
  SEARCH_BY = %w(accounts addresses providers phone_numbers sales_reps base_panels report_delivery_types)

  def initialize(
    order_table: 'accounts',
    order_column: 'prefix',
    order_direction: 'asc',
    terms: nil,
    offset: 0,
    limit: 10,
    requisition_number: nil,
    sales_rep_id: nil,
    search_by: "accounts"
  )
    self.order_table = order_table
    self.order_column = order_column
    self.order_direction = order_direction
    self.terms = terms
    self.offset = offset
    self.limit = limit
    self.requisition_number = requisition_number
    self.sales_rep_id = sales_rep_id
    fail "Invalid search by: #{search_by}" unless SEARCH_BY.include?(search_by)
    self.search_by = search_by
  end

  def accounts
    filtered_accounts.offset(offset).limit(limit)
  end

  def filtered_accounts
    return @filtered_accounts if @filtered_accounts
    @filtered_accounts = Account.distinct
                                .order("#{order_table}.#{order_column} #{order_direction}")
                                .joins(generate_joins)
    @filtered_accounts = @filtered_accounts.where(WHERE[search_by], terms: "%#{terms.upcase}%") if terms.present?
    @filtered_accounts = @filtered_accounts.where(sales_rep: sales_rep_id) if sales_rep_id.present?
    @filtered_accounts = find_requisition_number(@filtered_accounts) if requisition_number.present?
    @filtered_accounts
  end

  private
    attr_accessor :order_table, :order_column, :order_direction, :terms, :offset, :limit, :requisition_number, :sales_rep_id, :search_by

    def tables_to_be_loaded
      (
        JOIN_TABLES_DEPENDENCIES[search_by] +
        JOIN_TABLES_DEPENDENCIES[order_table]
      ).uniq
    end

    def generate_joins      
      tables_to_be_loaded.map { |table| JOINS[table] }.join(" ")
    end

    JOIN_TABLES_DEPENDENCIES = {
      "accounts" => %w(account_numbers),
      "account_numbers" => %w(account_numbers),
      "addresses" => %w(account_numbers addresses),
      "providers" => %w(account_providers providers),
      "phone_numbers" => %w(account_numbers sales_reps phone_numbers),
      "sales_reps" => %w(sales_reps),
      "base_panels" => %w(base_panels base_panel_names panel_adjustments panel_adjustment_names),
      "base_panel_names" => %w(base_panels base_panel_names),
      "report_delivery_types" => %w(account_report_delivery_types reporty_delivery_types),
    }

    JOINS = {
      "account_numbers" => '
        LEFT OUTER JOIN account_numbers ON
          accounts.id = account_numbers.account_id',

      "addresses" => '
        LEFT OUTER JOIN addresses ON
          account_numbers.id = addresses.addressable_id AND
          addresses.addressable_type = "AccountNumber"',

      "account_providers" => '
        LEFT OUTER JOIN account_providers ON
          accounts.id = account_providers.account_id',

      "providers" => '
        LEFT OUTER JOIN providers ON
          providers.id = account_providers.provider_id',

      "sales_reps" => '
        LEFT OUTER JOIN sales_reps
          ON sales_reps.id = accounts.sales_rep_id',

      "phone_numbers" => '
        LEFT OUTER JOIN phone_numbers ON
          (account_numbers.id = phone_numbers.phoneable_id AND phone_numbers.phoneable_type = "AccountNumber") OR
          (sales_reps.id = phone_numbers.phoneable_id AND phone_numbers.phoneable_type = "SalesRep")',

      "base_panels" => '
        LEFT OUTER JOIN base_panels ON
          accounts.id = base_panels.account_id',

      "base_panel_names" => '
        LEFT OUTER JOIN base_panel_names ON
          base_panel_names.id = base_panels.base_panel_name_id',

      "panel_adjustments" => '
        LEFT OUTER JOIN panel_adjustments ON
          base_panels.id = panel_adjustments.base_panel_id',

      "panel_adjustment_names" => '
        LEFT OUTER JOIN panel_adjustment_names ON
          panel_adjustment_names.id = panel_adjustments.panel_adjustment_name_id',

      "account_report_delivery_types" => '
        LEFT OUTER JOIN account_report_delivery_types ON
          accounts.id = account_report_delivery_types.account_id',

      "reporty_delivery_types" => '
        LEFT OUTER JOIN report_delivery_types ON
          account_report_delivery_types.report_delivery_type_id = report_delivery_types.id',
    }

    WHERE = {
      "accounts" => "
        UPPER(accounts.prefix) LIKE :terms OR
        UPPER(accounts.company_name) LIKE :terms OR
        UPPER(account_numbers.number) LIKE :terms",

      "addresses" => "
        UPPER(addresses.address_line_1) LIKE :terms OR
        UPPER(addresses.address_line_2) LIKE :terms OR
        UPPER(addresses.city) LIKE :terms OR
        UPPER(addresses.state) LIKE :terms OR
        UPPER(addresses.zip) LIKE :terms",

      "providers" => "
        UPPER(providers.name) LIKE :terms OR
        UPPER(providers.npi_number) LIKE :terms",

      "phone_numbers" => "
        UPPER(phone_numbers.number) LIKE :terms OR
        UPPER(phone_numbers.number_type) LIKE :terms",

      "sales_reps" => "
        UPPER(sales_reps.first_name) LIKE :terms OR
        UPPER(sales_reps.last_name) LIKE :terms OR
        UPPER(sales_reps.manager_title) LIKE :terms",

      "base_panels" => "
        UPPER(base_panel_names.name) LIKE :terms OR
        UPPER(panel_adjustment_names.name) LIKE :terms",

      "report_delivery_types" => "
        UPPER(report_delivery_types.name) LIKE :terms",
    }

    def find_requisition_number(scope)
      scope.joins(
        "LEFT OUTER JOIN requisition_ranges ON requisition_ranges.account_id = accounts.id"
      ).where(
        "requisition_ranges.starts_at <= :requisition_number AND
         requisition_ranges.ends_at >= :requisition_number",
        requisition_number: requisition_number
      )
    end
end
