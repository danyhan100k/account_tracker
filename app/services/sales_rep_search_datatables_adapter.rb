class SalesRepSearchDatatablesAdapter
  def initialize(params)
    self.params = params          
  end

  def as_json(*)          
    sales_rep_search = SalesRepSearch.new(
      order_column: order_column,
      order_direction: order_direction,
      terms: terms,
      offset: offset,
      limit: limit,
      sales_rep_id: sales_rep_id
    )      
    render sales_rep_search.sales_reps, sales_rep_search.filtered_sales_reps.count, order_column, order_direction
  end

  private

    attr_accessor :params
    def sales_rep_id
      params[:salesRepId]
    end

    def order      
      @order ||= params[:order]['0']
    end

    def order_column
      key = params[:columns][order[:column]][:data]
      ORDER_ATTRIBUTES[key]            
    end

    def order_direction
      order[:dir]            
    end

    def show_all?
      params[:length] == "-1"            
    end

    def terms
      @terms ||= params[:search][:value]            
    end

    def offset            
      return 0 if show_all?
      params[:start]            
    end

    def limit
      return SalesRep.count if show_all?
      params[:length]            
    end

    INCLUDES = [:manager, :accounts, :phone_numbers, :address]

    JSON_SCHEMA = {
      include: [:manager, :accounts, :phone_numbers, :address]
    }

    def render(sales_reps, records_filtered, order_column, order_direction)
      {
        draw: params[:draw],
        recordsTotal: SalesRep.count,
        recordsFiltered: records_filtered,
        data: sales_reps.includes(INCLUDES).as_json(JSON_SCHEMA) 
      }            
    end             
                  
  ORDER_ATTRIBUTES = {
    "full_name" => "sales_reps.first_name",
    "email" => "sales_reps.email",
    "manager" => "managers_sales_reps.first_name",
    "manager_title" => "sales_reps.manager_title",
    "phone_numbers" => "phone_numbers.number",
    "address" => "addresses.address_line_1",
    "accounts" => "accounts.prefix",
  }
end
