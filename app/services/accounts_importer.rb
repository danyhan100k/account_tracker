class AccountsImporter
  PHONE_NUMBER_REGEX = /
    (
      \(\d{3}\)
      \s{0,2}
      \d{3}
      [-.]
      \d{4}
    )
  /x

  class ImportError < StandardError
    attr_reader :errors

    def initialize(errors)
      @errors = errors
    end

    def to_s
      errors.map { |error| "#{error[:error][:message]}:\n#{error[:error][:backtrace].join("\n")}" }.join(", ")
    end
  end

  # excel version of the ultimate guide is the gold standard for:
  #   Account Addresses
  #   Account Providers
  #   Provider NPI Numbers
  #   Account Phone Numbers (and notes)
  #   Account Fax Numbers (and notes)
  #   Panels and modifications
  #   Report Deliveries

  PREFIX_COLUMN = 1
  ACCOUNT_NUMBERS_COLUMN = 2
  COMPANY_NAME_COLUMN = 3
  ADDRESSES_COLUMN = 4
  PROVIDER_COLUMN = 5
  NPI_NUMBER_COLUMN = 6
  PHONE_NUMBERS_COLUMN = 7
  FAX_NUMBERS_COLUMN = 8
  SALES_REP_COLUMN = 9
  BASE_PANEL_COLUMN = 10
  REPORT_DELIVERY_TYPE_COLUMN = 12
  def import(sheet)
    each_row(sheet) do |row|
      account = extract_account row
      extract_providers_and_npi_numbers(row).each do |provider|
        provider.accounts << account
        provider.save!
      end

      phone_numbers = extract_phone_numbers row[PHONE_NUMBERS_COLUMN], 'phone number'
      fax_numbers = extract_phone_numbers row[FAX_NUMBERS_COLUMN], 'fax'

      account_numbers = extract_account_numbers(row)
      raise "Must provide at least 1 account number" if account_numbers.empty?

      account_numbers
        .cycle
        .take([phone_numbers, fax_numbers].max.length)
        .zip(phone_numbers, fax_numbers) do |account_number, phone_number, fax_number|
          phone_number.update!(phoneable: account_number) if phone_number
          fax_number.update!(phoneable: account_number) if fax_number
        end

      sales_rep = extract_sales_rep(row)
      if sales_rep
        sales_rep.accounts << account
        sales_rep.save!
      end

      base_panel = extract_base_panel(row)
      if base_panel
        account.base_panels << base_panel
      end

      report_delivery_types = extract_report_delivery_types(row)
      if report_delivery_types
        account.report_delivery_types << report_delivery_types
      end
    end
  end

  private

  QUALIFIER_REGEX = /(#{PanelAdjustment.qualifiers.keys.join("|")}):/i

  def extract_base_panel(row)
    return unless row[BASE_PANEL_COLUMN].present?

    data = remove_html_from(row[BASE_PANEL_COLUMN])
    base_panel_name_name, *list = data.split(QUALIFIER_REGEX).reject(&:blank?)

    panel_adjustments = list.in_groups_of(2).map do |qualifier, panel_adjustment_name_name|
      extract_panel_adjustment(qualifier, panel_adjustment_name_name)
    end

    base_panel_name = extract_base_panel_name(base_panel_name_name)

    BasePanel.create! base_panel_name: base_panel_name, panel_adjustments: panel_adjustments
  end

  def extract_report_delivery_types(row)
    report_delivery_type_column = row[REPORT_DELIVERY_TYPE_COLUMN]
    return unless report_delivery_type_column.present? && report_delivery_type_column.is_a?(String)

    report_delivery_type_column
      .split("\n")
      .reject(&:blank?)
      .map { |report_delivery_type| report_delivery_type.strip.gsub(/:/, "") }
      .map do |report_delivery_type_name|
        ReportDeliveryType.find_or_create_by! name: report_delivery_type_name
      end
  end

  def remove_html_from(column)
    ActionView::Base.full_sanitizer.sanitize(column).gsub(/\n/, " ").strip.squeeze(" ")
  end

  def extract_base_panel_name(name)
    BasePanelName.find_or_create_by! name: name.strip.squeeze
  end

  def extract_panel_adjustment(qualifier, panel_adjustment_name_name)
    #qualifiers edge cases from excel sheet
    edge_cases = /(ONLY TEST FOR|TESTING ONLY|TEST ONLY)/i
    qualifier = "TEST ONLY" if qualifier =~ edge_cases

    panel_adjustment_name = extract_panel_adjustment_name(panel_adjustment_name_name)
    PanelAdjustment.create!(
      qualifier: qualifier.upcase,
      panel_adjustment_name: panel_adjustment_name
    )
  end

  def extract_panel_adjustment_name(panel_adjustment_name)
    panel_adjustment_name = panel_adjustment_name.strip

    PanelAdjustmentName.find_or_create_by! name: panel_adjustment_name
  end

  def extract_account(row)
    Account.find_by!(prefix: remove_html_from(row[PREFIX_COLUMN]))
  end

  def extract_providers_and_npi_numbers(row)
    providers = row[PROVIDER_COLUMN].to_s.split("\n").map(&:strip).reject(&:blank?)
    npi_numbers = row[NPI_NUMBER_COLUMN].to_s.split("\n").map(&:strip).reject(&:blank?)
    providers.zip(npi_numbers).map do |provider, npi_number|
      Provider.new name: provider, npi_number: npi_number
    end
  end

  ACCOUNT_NUMBER_REGEX = /\d{6}-?\s?\w{0,2}/
  def extract_account_numbers(row)
    return row[ACCOUNT_NUMBERS_COLUMN].to_s
           .split("\n")
           .map { |line| remove_html_from line }
           .select { |line| line =~ ACCOUNT_NUMBER_REGEX }
           .map { |account_number| account_number.gsub(" ", "") }
           .map do |account_number|
             AccountNumber.find_by! number: account_number
           end
  end

  def extract_phone_numbers(cell, number_type)
    #case1 = [["notes", "(123) 123-1234"], ["notes", "(123) 123-1234"]]
    #case2 = [["notes", "(123) 123-1234"], ["notes", nil]]
    #case3 = [["(123) 123-1234", "notes"], ["(123) 123-1234", "notes"]]
    #case4 = [["(123) 123-1234", "notes"], ["(123) 123-1234", nil]]
    #case5 = [["(123) 123-1234", "(123) 123-1234"]]
    #case6 = [["?", nil]]
    #case7 = [["N/A", nil]]
    cell
    .to_s
    .split(PHONE_NUMBER_REGEX)
    .map(&:presence)
    .compact
    .in_groups_of(2)
    .flat_map do |phone_number, notes| # case 3 & 4
      phone_number, notes = [notes, phone_number] if notes.to_s =~ PHONE_NUMBER_REGEX # case 1

      unless phone_number =~ PHONE_NUMBER_REGEX # case 2, 6 & 7
        phone_number, notes = [notes, phone_number]
      end

      if notes.to_s =~ PHONE_NUMBER_REGEX
        [
          PhoneNumber.new(number: phone_number, number_type: number_type),
          PhoneNumber.new(number: notes, number_type: number_type)
        ] # case 5
      else
        PhoneNumber.new(number: phone_number, notes: notes.to_s.strip, number_type: number_type)
      end
    end
  end

  def extract_sales_rep(row)
    return unless row[SALES_REP_COLUMN].is_a? String
    return unless row[SALES_REP_COLUMN].sub("\\", "").present?
    *first_name, last_name = row[SALES_REP_COLUMN].split(" ")
    first_name = first_name.join(" ").presence
    first_name, last_name = [last_name, first_name] if first_name.blank?
    SalesRep.new(first_name: first_name, last_name: last_name)
  end

  def each_row(sheet)
    errors = []
    ActiveRecord::Base.transaction do
      sheet.drop(1).each_with_index do |row, row_number|
        begin
          yield row
        rescue => e
          errors << {
            error: {
              message: e.message,
              backtrace: e.backtrace
            },
            row: row, row_number: row_number
          }
        end
      end

      raise ImportError.new(errors) if errors.any?
    end
  end
end
