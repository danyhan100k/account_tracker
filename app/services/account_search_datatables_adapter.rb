class AccountSearchDatatablesAdapter
  def initialize(params)
    self.params = params          
  end

  def as_json(*)          
    account_search = AccountSearch.new(
      order_column: order_column,
      order_direction: order_direction,
      order_table: order_table,
      terms: terms,
      offset: offset,
      limit: limit,
      requisition_number: requisition_number,
      sales_rep_id: sales_rep_id,
      search_by: search_by
    )
    render account_search.accounts, account_search.filtered_accounts.size
  end

  private

    attr_accessor :params

    def order      
      @order ||= params[:order]['0']
    end

    def order_header
      params[:columns][order[:column]][:data]
    end

    def order_table
      ORDER_ATTRIBUTES[order_header][:table]
    end

    def order_column
      ORDER_ATTRIBUTES[order_header][:column]
    end

    def order_direction
      order[:dir]            
    end

    def show_all?
      params[:length] == "-1"            
    end

    def terms
      @terms ||= params[:search][:value]            
    end

    def offset  
      return 0 if show_all?
      params[:start]            
    end

    def limit
      return Account.count if show_all?
      params[:length]            
    end

    def requisition_number   
      params[:requisitionNumber]            
    end

    def sales_rep_id
      params[:salesRepId]
    end

    def search_by
      params[:searchBy].presence || 'accounts'
    end

    INCLUDES = [
      :report_delivery_types,
      :providers,
      :requisition_ranges,
      account_numbers: [
        :phone_numbers,
        :addresses
      ],
      sales_rep: [:phone_numbers],
      base_panels: [
        :base_panel_name,
        panel_adjustments: [:panel_adjustment_name]
      ]
    ]

    JSON_SCHEMA = {
      include: {
        providers: {},
        report_delivery_types: {},
        requisition_ranges: {},
        account_numbers: {
          include: [:phone_numbers, :addresses]
        },
        base_panels: {
          include: [
            :base_panel_name,
            panel_adjustments: {
              include: :panel_adjustment_name
            }
          ]
        },
        sales_rep: {
          include: :phone_numbers
        }
      }
    }

    def render(accounts, records_filtered)            
      {
        draw: params[:draw],
        recordsTotal: Account.count,
        recordsFiltered: records_filtered,
        data: accounts.includes(INCLUDES).as_json(JSON_SCHEMA)
      }            
    end

    ORDER_ATTRIBUTES = {
      "prefix" => {
        table: "accounts",
        column: "prefix"
      },
      "company_name" => { 
        table: "accounts",
        column: "company_name"
      },
      "sales_rep" => { 
        table: "sales_reps",
        column: "first_name"
      },
      "has_sample_collector" => {
        table: "accounts",  
        column: "has_sample_collector"
      },
    }
end
