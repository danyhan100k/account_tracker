class SalesRepSearch
  def initialize(
    order_column: 'sales_reps.first_name',
    order_direction: 'asc',
    terms: nil,
    offset: 0,
    limit: 10, 
    sales_rep_id: nil
  )
    self.order_column = order_column
    self.order_direction = order_direction
    self.terms = terms
    self.offset = offset
    self.limit = limit
    self.sales_rep_id = sales_rep_id
  end

  def sales_reps
    filtered_sales_reps.offset(offset).limit(limit)
  end

  def filtered_sales_reps
    return @filtered_sales_reps if @filtered_sales_reps
    @filtered_sales_reps = SalesRep.distinct
    @filtered_sales_reps = @filtered_sales_reps
                             .includes(:accounts, :manager, :phone_numbers, :address)
                             .references(:accounts, :manager, :phone_numbers, :address)
    @filtered_sales_reps = @filtered_sales_reps.where(WHERE, terms: "%#{terms}%") if terms.present?
    @filtered_sales_reps.order("#{order_column} #{order_direction}") 
  end

  private
    attr_accessor :order_column, :order_direction, :terms, :offset, :limit, :sales_rep_id

    WHERE = %w(
      sales_reps.first_name
      sales_reps.last_name
      sales_reps.email
      managers_sales_reps.first_name
      managers_sales_reps.last_name
      managers_sales_reps.email
      sales_reps.manager_title
      phone_numbers.number
      addresses.address_line_1
      addresses.address_line_2
      addresses.city
      addresses.state
      addresses.zip
      accounts.prefix
    ).map{ |column| "UPPER(#{column}) LIKE UPPER(:terms)" }.join(" OR ")
end
