// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//=  require jquery
//=  require dataTables/jquery.dataTables
//=  require turbolinks
//=  require bootstrap-sprockets
//=  require maskedinput
//=  require 'rails_admin/jquery.remotipart.fixed'
//=  require 'jquery-ui/effect'
//=  require 'jquery-ui/sortable'
//=  require 'jquery-ui/autocomplete'
//=  require 'rails_admin/moment-with-locales'
//=  require 'rails_admin/bootstrap-datetimepicker'
//=  require 'rails_admin/jquery.colorpicker'
//=  require 'rails_admin/ra.filter-box'
//=  require 'rails_admin/ra.filtering-multiselect'
//=  require 'rails_admin/ra.filtering-select'
//=  require 'rails_admin/ra.remote-form'
//=  require 'rails_admin/jquery.pjax'
//=  require 'jquery_nested_form'
//=  require 'rails_admin/ra.nested-form-hooks'
//=  require 'rails_admin/ra.i18n'
//=  require 'rails_admin/bootstrap/bootstrap'
//=  require 'rails_admin/ra.widgets'
//=  require 'rails_admin/ui'
//=  require 'rails_admin/custom/ui'
//=  require semantic-ui
//=  require_tree ../../../vendor/assets/javascripts/.
//=  require_self
//=  require_tree .
