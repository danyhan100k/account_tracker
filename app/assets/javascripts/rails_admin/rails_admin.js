//=  require ../application.js

(function($){
   $("[data-phone-number]").mask("99/99/9999",{placeholder:"mm/dd/yyyy"});
})(jQuery);
$(document).ready(function(){
   
   //need to remove the tab dynamically if it is added or else the form
   //doesn't validate and won't save. The kicker here is that rails admin
   //doesn't really add the stuff to the dom on the click
   //so a nested field inside a nested field won't hook, constantly querying
   //the dom will take care of this, and speed isnt' a concern on this page
   //so it should be good and make the user's life better
   setInterval(remove_nested_fields,1000);
   //trimming the values so when Jesus copies and pastes
   //the form doesn't think he is wrong
   $("#edit_account").submit(function(){
    $('input').val(function(_, value) {
      return $.trim(value);
    });      
   });
});
function remove_nested_fields(){
  //give the dom a second to load
  setTimeout(function(){
   $(document).find('.remove_nested_fields').on('click',function(event){
     event.stopImmediatePropagation();
     var $currentTab = $(this).closest(".tab-content");
     $currentTab.siblings('.controls').find('.nav-tabs').find("li.active").fadeOut("slow",function(){
       $currentTab.find('div.active').fadeOut("slow").remove();
     }).remove();
   });
  },1000);
}
