function setupSalesRepsSearch() {
  //this should be a singleton and only called when this route is displayed
  if(window.location.pathname === "/sales_reps" && $.fn.dataTable.isDataTable($('#sales-reps')) === false ){
    var salesRepsTable = $('#sales-reps').DataTable({
      processing: true,
      serverSide: true,
      ajax: {
        url: '/sales_reps.json'
      },
      dom: 'Bfrtip',
      pageLength: '15',
      aLengthMenu: [
        [10,50,100, -1],
        ['10 rows', '50 rows', '100 rows', 'Show all']
      ],
      buttons: ['copy', 'pdf', 'print', 'excelHtml5', 'pageLength'],
      columns: [
         { data: "full_name", name: "Full name", render: renderFullName },
         { data: "email", name: "Email" },
         { data: "manager", name: "Manager", render: renderManager }, 
         { data: "manager_title", name: "Manager title" },
         { data: "phone_numbers", name: "Phone numbers", render: phoneNumbers },
         { data: "address", name: "Address", render: renderAddress },
         { data: "accounts", name: "Accounts prefixes", render: renderAccounts },
       ]
    });//end of salesreps 

    function renderFullName(_, _, salesRep) {
      var id = salesRep.id
      var full_name = [salesRep.first_name, salesRep.last_name].filter(Boolean).join(" ");
      return "<a href='" + window.location.origin + "/sales_reps/" + id + "'>" + full_name + "</a>"
    }

    function phoneNumbers(_,_,row) {

      var num_lis = [];
      if (row.phone_numbers) { 
        row.phone_numbers.map(function(number){
          if (number.number_type) {
            var li = "<li>" + number.number + "(" + number.number_type + ")" + "</li>"; 
          } else {
            var li = "<li>" + number.number + "</li>"
          }
          num_lis.push(li);
        });
      }
      if (num_lis.length > 0) {
       return "<ul>" + num_lis.join('<br>') + "</ul>"
      } else {
       return ''
      }
    };

    function renderManager(manager) {
      if (manager) {
        return manager.first_name + " " + manager.last_name + " (" + manager.email + ")";
      }
      return "";
    }

    function renderAddress(_, _, salesRep) {
      return [
        salesRep.address_line_1,
        salesRep.address_line_2,
        salesRep.city,
        salesRep.state,
        salesRep.zip
      ].filter(Boolean).join(", ") ;
    }

    function renderAccounts(accounts) {
      return accounts.map(function(account) { return account.prefix; }).join(", ");
    }

  }//end of pathname if
}
$(document).on('turbolinks:load', setupSalesRepsSearch);
