$(document).on('turbolinks:load', setupDropdown);

function setupDropdown(){
  $(".navigation.dropdown").dropdown({
    action: navigateTo,
  });
}

function navigateTo(_, href) {
  Turbolinks.visit(href);
}
