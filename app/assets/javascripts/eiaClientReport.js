$(document).on('turbolinks:load', function(){

	function generateMessage(found_count){
		return '<div class="ui statistics">' + 
	      '<div class="statistic">' + 
	        '<div class="value">' + 
	         found_count +
	        '</div>' + 
	        '<div class="label">' + 
	          'Specimen Found' + 
	        '</div>' + 
	      '</div>' + 
	    '</div>';
	};

	function generateCsvButton(message,url) {
		return '<div>' + message + 
						'<a class="generate-csv-link" href="' + url + '">' + 
						'<button class="fluid ui button big teal">Generate CSV</button>' + 
						'</a>' + 
						'</div>';
	};

	var datePicker = $('#dp1').datepicker()
	datePicker.on('changeDate', function(ev){
		$('.datepicker').hide();
			var datePicked = ev.date.valueOf();
			var convertedDate = moment(datePicked).utc().format('YYYY-MM-DDTHH:mm:ssZZ')
			var data = {
				specimen_date_received: convertedDate
			};
			$.get('/specimens/check_for_report.json', data, function onSuccess(response){
				$('.datepicker-result').html(function(){
					var message = generateMessage(response.found_count);
					var generateReportUrl = "generate_report.csv?id=";
					if (response.found_count > 0) {
						//TODO: USE WHILE or FOR loop to generate url instead of map
						response.id.map(function(id){
							generateReportUrl = generateReportUrl + id + ","
						});
						return generateCsvButton(message, generateReportUrl);
					} else {
						return message;
					}
				});
			}); //get
		});

});

