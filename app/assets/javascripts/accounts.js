function setupAccountsSearch() {
  //need to refactor this using factory method
   var current_path = window.location.pathname;

  //the datatables methods do not need to be called on the admin side
  //it slows down performance and propagates errors
  //and we also need to make sure that it is a singleton

  var requisitionNumberInput = $("#requisition-number");
  var searchBySelect = $("#search-by");
  var accountsTable;
  if(current_path === "/" && $.fn.dataTable.isDataTable($("#example")) === false ){
   accountsTable = renderAccountsTable(function() {
    return {
      requisitionNumber: requisitionNumberInput.val(),
      searchBy: searchBySelect.val()
    };
  });
 }

 //since this is now a singleton we have to manually
 //control whether or not we are going to draw
 //this keeps the global memory community clean
 //and not polluted

 if(accountsTable !== undefined){
   searchBySelect.change(accountsTable.draw);
   requisitionNumberInput.on('input', accountsTable.draw);
 }

  var salesRepSelect = $("#sales-rep");
  salesRepSelect.one('change', function(){
    $.get('/accounts/accounts_search_table').then(function (html) {
      $(".table-wrapper").html(html);
      var accountsTable = renderAccountsTable(function () {
        return {
          salesRepId: salesRepSelect.val(),
          searchBy: $("#search-by").val()
        };
      });
      salesRepSelect.on('change', accountsTable.draw);
    });
  });

  function renderAccountsTable(paramsFn) {
    return $('#example').DataTable({
      processing: true,
      serverSide: true,
      ajax: {
        url: '/accounts.json',
        data: function(data) {
          return $.extend({}, paramsFn(), data);
        }
      },
      dom: 'Bfrtip',
      pageLength: '15',
      aLengthMenu: [
        [10,50,100, -1],
        ['10 rows', '50 rows', '100 rows', 'Show all']
      ],
      buttons: ['copy', 'pdf', 'print', 'excelHtml5', 'pageLength'],
      columns: [
        { data: "prefix", name: "Prefix", createdCell: colorPrefixes },
        { data: "account_numbers", name: "Account", render: renderAccountNumbersList, orderable: false },
        { data: "company_name", name: "Company" },
        { data: "account_numbers", name: "Address", render: renderAddressesList, orderable: false },
        { data: "providers", name: "Providers & NPI Number", render: renderProvidersList, orderable: false },
        { data: "phone_numbers", name: "Contact", render: renderPhoneNumbersList, orderable: false },
        { data: "sales_rep", name: "Account Rep", render: renderSalesRep },
        { data: "base_panels", name: "Panel Adjustment", render: renderBasePanels, orderable: false },
        { data: "report_delivery_types", name: "Report Delivery", render: renderReportDeliveryTypes, orderable: false },
        { data: "has_sample_collector", name: "Sample Collector", render: renderSampleCollector }
      ]
    });
  } //function renderAccountsTable

  function filter_true_values(arr) {
   return arr.filter(Boolean);
  }

  function renderAccountNumbersList(accountNumbers) {
    return renderList_ul(renderAccountNumber_li, accountNumbers);
  }

  function renderAddressesList(accountNumbers) {
    return renderList_ul(renderAccountNumberAddresses_li, accountNumbers);
  }

  function renderProvidersList(providers) {
    return renderList_ul(renderProvider_li, providers);
  }

  function renderPhoneNumbersList(phoneNumbers, _, account) {
    var accountNumbers = [];
    account.account_numbers.forEach(function(account_number){
      var new_account = {}; //creating new phone number object with account number and phone numbers
      new_account.account_number = account_number.number;
      new_account.phone_numbers = account_number.phone_numbers.map(function(phone_number) { 
                                                                        return phone_number.number + ' (' + phone_number.number_type + ')' 
                                                                  });
      accountNumbers.push(new_account);
    });
    return renderContactList(accountNumbers);
  }

  function renderContactList(accountNumbers) {
    var contact_list = [];
    accountNumbers.forEach(function(acc_number) {
          var phone_numbers = filter_true_values(acc_number.phone_numbers);
          if (phone_numbers.length > 0) { 
            var number_li = acc_number.phone_numbers.map(function(phone_number) {
                                return "<br>" + "<li>" + phone_number + "</li>"
                            });

            var contact = "<br>" + "<ul>" +
                            "<h5>" + "Account # " + acc_number.account_number +"</h5>" +
                                number_li.join('') +
                          "</ul>";
          } else {
            var contact = "<br>" + "<h5>" + "Account # " + acc_number.account_number +"</h5>"
          }          
          contact_list.push(contact);
    });
    return contact_list.join('');
  };

  function renderSalesRep(salesRep) {
    if (!salesRep) { return ''; }
    var lastName = salesRep.last_name || "";
    var fullName = salesRep.first_name + " " + lastName;
    var phoneNumbers_ul =  "<ul>" +
                            "<li>" + salesRep.email + "</li>" +
                            salesRep.phone_numbers
                                          .map(function(number) { return "<li>" + number.number + "</li>" })
                                          .join('') +
                           "</ul>";
    if (salesRep.manager_title) {
      return fullName + "(" + salesRep.manager_title + ")" + phoneNumbers_ul;
    } else {
      return fullName + phoneNumbers_ul;
    }
  }

  function renderBasePanels(basePanels) {
    return renderList_ul(renderBasePanel_li, basePanels);
  }

  function renderReportDeliveryTypes(reportDeliveryTypes, _, row) {
    var li_s = ""
    reportDeliveryTypes.forEach(function(deliveryType){
      li_s = li_s.concat(renderReportDeliveryType_li(deliveryType, row.prefix))
    });
    return "<ul>" + li_s + "</ul>";
  }

  function renderList_ul(renderAccountNumber_li, collection, row) {
    return "<ul>" + collection.map(renderAccountNumber_li).join('') + "</ul>";
  }

  function renderAccountNumber_li(accountNumber) {
    return "<li>" + accountNumber.number + "</li>";
  }

  function renderAccountNumberAddresses_li(accountNumber) {
    return  "<h5>" + "Acc#: " + accountNumber.number + "</h5>" +
      renderList_ul(renderAddress_li, accountNumber.addresses);
  }

  function renderAddress_li(address) {
    var unfiltered_address = [ address.address_line_1, address.address_line_2, address.city, address.state, address.zip];
    var filtered_address = filter_true_values(unfiltered_address);
    var final_address = "";

    for (i in filtered_address) { 
      final_address += " " + filtered_address[i];
    }

    return "<br>" + "<li>" +            
            final_address + 
           "</li>";
  }

  function renderProvider_li(provider) {
    return "<li>" + provider.name + " - " + provider.npi_number + "</li>";
  }

  function renderPhoneNumber_li(phoneNumber) {
    return "<li>" + phoneNumber.number + " (" + phoneNumber.number_type + ")</li>";
  }

  function renderBasePanel_li(basePanel) {
    return "<li>" + "<strong>" + basePanel.base_panel_name.name + "</strong>" + renderList_ul(renderPanelAdjustment_li, basePanel.panel_adjustments) + "</li>";
  }

  function renderPanelAdjustment_li(panelAdjustment) {
    return "<li>" + panelAdjustment.qualifier + " - " + panelAdjustment.panel_adjustment_name.name + "</li>";
  }

  function renderReportDeliveryType_li(reportDeliveryType, prefix) {
    var matchCondition = /online\sclient/i;
    var link = "https://mdlabs-dmaa.mdlabs.local/SecretServer/dashboard.aspx?term=" + prefix.toLowerCase();
    if (reportDeliveryType.name.match(matchCondition)) {
      return "<br><li>" + "<a href='" + link + "'>"+ reportDeliveryType.name + "</a>" + "</li>";
    } else {
      return "<br><li>" + reportDeliveryType.name + "</li>";
    }
  }

  function colorPrefixes(cell, prefix) { 
    $(cell).css('background-color', generateColorFor(prefix));
  }

  function generateColorFor(prefix) {
    var group = prefix.slice(0, prefix.length - 1);
    return randomColor({ seed: group, luminosity: 'light' });
  }

  function renderSampleCollector(sample_collector) {
    var boolean_value = String(sample_collector);
    return boolean_value[0].toUpperCase() + boolean_value.slice(1)
  }

  function renderRequisitionRanges(requisitionRanges) {
    return renderList_ul(renderRequisitionRange_li, requisitionRanges);
  }

  function renderRequisitionRange_li(requisitionRange) {
    return "<li>" + requisitionRange.starts_at + " - " + requisitionRange.ends_at + "</li>";
  }
}//end of setup

$(document).on('turbolinks:load', setupAccountsSearch);
