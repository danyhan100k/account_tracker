# == Schema Information
#
# Table name: panel_adjustments
#
#  id                       :integer          not null, primary key
#  panel_adjustment_name_id :integer
#  base_panel_id            :integer
#  qualifier                :integer
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#
# Indexes
#
#  index_panel_adjustments_on_base_panel_id             (base_panel_id)
#  index_panel_adjustments_on_panel_adjustment_name_id  (panel_adjustment_name_id)
#
# Foreign Keys
#
#  fk_rails_27ee9df799  (panel_adjustment_name_id => panel_adjustment_names.id)
#  fk_rails_559b1a8a54  (base_panel_id => base_panels.id)
#

class PanelAdjustment < ActiveRecord::Base
  belongs_to :panel_adjustment_name
  belongs_to :base_panel, inverse_of: :panel_adjustments
  enum qualifier: ["TEST_PANEL", "NO", "TEST", "TEST ONLY", "YES", "TEST IF DECLARED", "OTHER", "DO NOT TEST FOR", "EIA", "ADDITIONAL TESTING", "MISSING QUALIFIER"]

  validates_presence_of :panel_adjustment_name, :qualifier,# :base_panel

  def name
    if panel_adjustment_name.present?
      "#{qualifier} - #{panel_adjustment_name.name}"
    else
      self.class.name
    end
  end
end
