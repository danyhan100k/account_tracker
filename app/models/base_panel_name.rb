# == Schema Information
#
# Table name: base_panel_names
#
#  id         :integer          not null, primary key
#  name       :text(65535)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class BasePanelName < ActiveRecord::Base
	has_many :base_panels
	validates_presence_of :name
end
