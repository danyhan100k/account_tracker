# == Schema Information
#
# Table name: result_sets
#
#  id              :integer          not null, primary key
#  result_set_code :string(255)
#  description     :string(255)
#  drug_class      :string(255)
#  billing_class   :string(255)
#  billing_code    :string(255)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class ResultSet < ActiveRecord::Base
  has_many :order_sets_result_set
  has_many :order_sets, through: :order_sets_result_set
end
