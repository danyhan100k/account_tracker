# == Schema Information
#
# Table name: account_numbers_order_sets
#
#  id                :integer          not null, primary key
#  account_number_id :integer
#  order_set_id      :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class AccountNumbersOrderSet < ActiveRecord::Base
  belongs_to :account_number
  belongs_to :order_set
end
