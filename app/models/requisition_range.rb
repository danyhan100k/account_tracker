# == Schema Information
#
# Table name: requisition_ranges
#
#  id         :integer          not null, primary key
#  starts_at  :integer
#  ends_at    :integer
#  account_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_requisition_ranges_on_account_id  (account_id)
#
# Foreign Keys
#
#  fk_rails_f3b5c7eced  (account_id => accounts.id)
#

class RequisitionRange < ActiveRecord::Base
  belongs_to :account, :inverse_of => :requisition_ranges
  validates_presence_of :account
  validate :validate_starts_at_less_than_or_equals_ends_at
  validate :validate_no_overlap

  def account_enum
    Account.all.map(&:name)
  end

  private 
  
  def validate_presence_of_starts_at_ends_at
    if starts_at.blank? || ends_at.blank?
      errors.add(:starts_at, "can't be blank") if starts_at.blank? && errors[:starts_at].blank?
      errors.add(:ends_at, "can't be blank") if ends_at.blank? && errors[:ends_at].blank?
      return true
    end 
    false
  end

  def validate_starts_at_less_than_or_equals_ends_at
    return if validate_presence_of_starts_at_ends_at
    if starts_at > ends_at
      errors.add(:starts_at, "starts_at must be less than or equals than ends_at")
    end
  end

  def validate_no_overlap
    return if validate_presence_of_starts_at_ends_at
    requisitions =  RequisitionRange.where(
      "(starts_at <= :starts_at AND ends_at >= :starts_at) OR
       (starts_at <= :ends_at AND ends_at >= :ends_at) OR

       (:starts_at <= starts_at AND :ends_at >= starts_at) OR
       (:starts_at <= ends_at AND :ends_at >= ends_at)",
      starts_at: starts_at,
      ends_at: ends_at,
    )
    if requisitions.reject { |req| req == self }.any?
      errors.add(:starts_at, "range is taken")
      errors.add(:ends_at, "range is taken")
    end
  end
end
