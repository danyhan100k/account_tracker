# == Schema Information
#
# Table name: sales_reps
#
#  id            :integer          not null, primary key
#  first_name    :string(255)
#  last_name     :string(255)
#  email         :string(255)
#  manager_id    :integer
#  manager_title :string(255)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  auth_token    :string(255)
#
# Indexes
#
#  index_sales_reps_on_manager_id  (manager_id)
#

class SalesRep < ActiveRecord::Base
  belongs_to :manager, class_name: 'SalesRep'
  has_many :phone_numbers, as: :phoneable
  has_many :accounts
  has_one :address, as: :addressable, dependent: :destroy

  accepts_nested_attributes_for :address, allow_destroy: true
  accepts_nested_attributes_for :phone_numbers, allow_destroy: true
  validates_presence_of :first_name, :last_name

  def name
    "#{first_name} #{last_name}"
  end

end
