# == Schema Information
#
# Table name: addresses
#
#  id               :integer          not null, primary key
#  addressable_id   :integer
#  addressable_type :string(255)
#  address_line_1   :string(255)
#  address_line_2   :string(255)
#  city             :string(255)
#  state            :string(255)
#  zip              :string(255)
#  notes            :string(255)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#
# Indexes
#
#  index_addresses_on_addressable_type_and_addressable_id  (addressable_type,addressable_id)
#

class Address < ActiveRecord::Base
  belongs_to :addressable, polymorphic: true
  validates_presence_of :address_line_1, :city, :state, :zip
  validates_format_of :zip, with: /\A\d{5}(?:[-\s]\d{4})?\z/, message: "Incorrect format on ZIP code. ex) 70077 or 70077-7777"
  
  def name
    address_line_1
  end
end
