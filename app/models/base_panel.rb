# == Schema Information
#
# Table name: base_panels
#
#  id                 :integer          not null, primary key
#  base_panel_name_id :integer
#  account_id         :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#
# Indexes
#
#  index_base_panels_on_account_id          (account_id)
#  index_base_panels_on_base_panel_name_id  (base_panel_name_id)
#
# Foreign Keys
#
#  fk_rails_7e7a1cf640  (account_id => accounts.id)
#  fk_rails_afecdf33fa  (base_panel_name_id => base_panel_names.id)
#

class BasePanel < ActiveRecord::Base
  belongs_to :base_panel_name
  belongs_to :account, inverse_of: :base_panels
  has_many :panel_adjustments, inverse_of: :base_panel, dependent: :destroy

  validates_presence_of :base_panel_name#, :account

  accepts_nested_attributes_for :panel_adjustments, allow_destroy: true

  def name
    if base_panel_name.present?
      base_panel_name.name
    else
      self.class.name
    end
  end
end
