# == Schema Information
#
# Table name: panel_adjustment_names
#
#  id         :integer          not null, primary key
#  name       :text(65535)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class PanelAdjustmentName < ActiveRecord::Base
	has_many :panel_adjustments
	validates_presence_of :name
end
