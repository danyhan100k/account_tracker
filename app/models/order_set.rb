# == Schema Information
#
# Table name: order_sets
#
#  id             :integer          not null, primary key
#  order_set_code :string(255)
#  description    :string(255)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class OrderSet < ActiveRecord::Base
  has_many :account_numbers_order_set
  has_many :account_numbers, through: :account_numbers_order_set
  has_many :order_sets_result_set
  has_many :result_sets, through: :order_sets_result_set
end
