# == Schema Information
#
# Table name: order_sets_result_sets
#
#  id            :integer          not null, primary key
#  order_set_id  :integer
#  result_set_id :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class OrderSetsResultSet < ActiveRecord::Base
  belongs_to :order_set
  belongs_to :result_set
end
