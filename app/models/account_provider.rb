# == Schema Information
#
# Table name: account_providers
#
#  id          :integer          not null, primary key
#  account_id  :integer
#  provider_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_account_providers_on_account_id   (account_id)
#  index_account_providers_on_provider_id  (provider_id)
#
# Foreign Keys
#
#  fk_rails_5160ecc4e0  (provider_id => providers.id)
#  fk_rails_5dd7c0e0b5  (account_id => accounts.id)
#

class AccountProvider < ActiveRecord::Base
  belongs_to :account
  belongs_to :provider

  validates_presence_of :account, :provider
end
