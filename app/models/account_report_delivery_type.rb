# == Schema Information
#
# Table name: account_report_delivery_types
#
#  id                      :integer          not null, primary key
#  account_id              :integer
#  report_delivery_type_id :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#
# Indexes
#
#  index_account_report_delivery_types_on_account_id               (account_id)
#  index_account_report_delivery_types_on_report_delivery_type_id  (report_delivery_type_id)
#
# Foreign Keys
#
#  fk_rails_a57b9a0eb0  (account_id => accounts.id)
#  fk_rails_edd74d4218  (report_delivery_type_id => report_delivery_types.id)
#

class AccountReportDeliveryType < ActiveRecord::Base
  belongs_to :account
  belongs_to :report_delivery_type

  validates_presence_of :account, :report_delivery_type
end
