# == Schema Information
#
# Table name: specimens
#
#  id                 :integer          not null, primary key
#  date_received      :date
#  batch              :integer          default(1), not null
#  account_id         :integer
#  requisition_number :string(255)      not null
#  is_processable     :boolean          default(TRUE), not null
#  is_processable_eia :boolean          default(TRUE), not null
#  is_wet             :boolean          default(FALSE), not null
#  is_stat            :boolean          default(FALSE), not null
#  notes_lab          :text(65535)
#  notes_clerical     :text(65535)
#  notes_sales_rep    :text(65535)
#  notes_other        :text(65535)
#  sample_type_id     :integer          not null
#  insurance_name     :string(255)
#  provider_name      :string(255)
#  age                :integer
#  date_processed     :datetime
#  date_released      :datetime
#  created_by         :string(255)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#
# Indexes
#
#  index_specimens_on_account_id  (account_id)
#
# Foreign Keys
#
#  fk_rails_c454d0c8b4  (account_id => accounts.id)
#

class Specimen < ActiveRecord::Base
  belongs_to :account, inverse_of: :specimens
  validates_presence_of :date_received, :batch, :requisition_number, :sample_type_id, :account, :created_by, :account_prefix
  validates_uniqueness_of :requisition_number
  accepts_nested_attributes_for :account

	def self.find_for_eia_report(date_received:)
		# [x] have a date_received equal to the date_received parameter provided by the user,
		# [x] specimen's account's base panel name does not include the text 'NO EIA'
		# [x] have an attribute of processable = true
		# [x] have an attribute of is_processable_eia = true
		# [x] specimen's account shouldn't have a panel adjustment name of 'EIA' with a qualifier of 'NO'
		#TODO: edge case
		# [] there's a base panel name with "Customized Pain Management Panel (with modifications) NO THC (EIA)", which breaks the where clause
		specimens_relation = Specimen.joins(:account => { :base_panels => [ :base_panel_name, { :panel_adjustments => :panel_adjustment_name } ]})
													        .where("
													        	specimens.date_received = :date
													        	AND specimens.is_processable = true
													        	AND specimens.is_processable_eia = true 
													        	AND UPPER(base_panel_names.name) NOT LIKE UPPER(:base_panel_name)
													        	AND NOT (
													        		UPPER(panel_adjustment_names.name) = UPPER(:panel_adjustment_name)
													        		AND panel_adjustments.qualifier = :qualifier
													          )
													        ", date: date_received, base_panel_name: "%NO EIA%", panel_adjustment_name: "EIA", qualifier: 1)
													        .distinct
		#[x] if specimen.account has_many panel_adjustments 
		#specimen will still be included in the result even if one of panel_adjustment
		#does not pass above filter.
		ids_to_remove = []
		specimens_relation.each do |specimen|
			account = specimen.account
			base_panels = account.base_panels
			base_panels.each do |base_panel|
				panel_adjustments = base_panel.panel_adjustments
				panel_adjustments.each do |panel_adjustment|
					if panel_adjustment.qualifier.upcase == 'NO' && panel_adjustment.panel_adjustment_name.name.upcase =~ /EIA/
						ids_to_remove << specimen.id
					end
				end
			end
		end
		if ids_to_remove.any?
			ids_to_remove_s = ids_to_remove.join(',')
			specimens_relation = specimens_relation.where("specimens.id NOT IN (#{ids_to_remove_s})")
		end
		return specimens_relation
	end

	def account_prefix
		account && account.prefix
	end
	def account_prefix=(prefix)
		account = Account.find_by(prefix: prefix)
		self.account = account
	end

  def as_json(options = {})
    super({ methods: :account_prefix }.merge(options))
  end
end
