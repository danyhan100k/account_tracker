# == Schema Information
#
# Table name: account_numbers
#
#  id         :integer          not null, primary key
#  account_id :integer
#  number     :string(255)
#  notes      :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_account_numbers_on_account_id  (account_id)
#
# Foreign Keys
#
#  fk_rails_980f2ec07b  (account_id => accounts.id)
#

class AccountNumber < ActiveRecord::Base
  belongs_to :account, inverse_of: :account_numbers
  has_many :addresses, as: :addressable, dependent: :destroy
  has_many :phone_numbers, as: :phoneable
  has_many :account_numbers_order_set
  has_many :order_sets, through: :account_numbers_order_set

  accepts_nested_attributes_for :addresses, allow_destroy: true
  accepts_nested_attributes_for :phone_numbers, allow_destroy: true
  validates :number, :account, presence: true
  def name
  	number
  end


end
