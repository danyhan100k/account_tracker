# == Schema Information
#
# Table name: accounts
#
#  id                   :integer          not null, primary key
#  prefix               :string(255)
#  company_name         :string(255)
#  notes                :string(255)
#  sales_rep_id         :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  has_sample_collector :boolean          default(FALSE)
#
# Indexes
#
#  index_accounts_on_sales_rep_id  (sales_rep_id)
#
# Foreign Keys
#
#  fk_rails_01911985a2  (sales_rep_id => sales_reps.id)
#

class Account < ActiveRecord::Base
  belongs_to :sales_rep
  has_many :account_providers
  has_many :providers, through: :account_providers
  has_many :base_panels, dependent: :destroy, inverse_of: :account
  has_many :account_numbers, dependent: :destroy, inverse_of: :account
  has_many :phone_numbers, through: :account_numbers
  has_many :addresses, through: :account_numbers
  has_many :account_report_delivery_types
  has_many :report_delivery_types, through: :account_report_delivery_types
  has_many :requisition_ranges, :inverse_of => :account
  has_many :specimens, dependent: :destroy, inverse_of: :account
  accepts_nested_attributes_for :base_panels, allow_destroy: true
  accepts_nested_attributes_for :account_numbers, allow_destroy: true
  accepts_nested_attributes_for :requisition_ranges, allow_destroy: true

  validates_presence_of :prefix #, :sales_rep
  validates_length_of :prefix, :notes, :company_name, maximum: 255
  validates_uniqueness_of :prefix

  def self.render_by_req_num(requisition_number)
    requisition_range = RequisitionRange.find_by!(
      "starts_at <= :req_number AND ends_at >= :req_number",
      :req_number => requisition_number
    )

    return JSON.pretty_generate(
      requisition_range.account.as_json(
        include: [
          :sales_rep,
          :providers,
          :base_panels,
          :account_numbers,
          :phone_numbers,
          :addresses,
          :report_delivery_types,
          :requisition_ranges
        ]
      )
    )
  end

  def prefix=(prefix)
    super prefix.to_s.upcase
  end

  def name
    "#{prefix} - #{company_name}"
  end
end
