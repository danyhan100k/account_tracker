# == Schema Information
#
# Table name: report_delivery_types
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class ReportDeliveryType < ActiveRecord::Base
	has_many :account_report_delivery_types
	has_many :accounts, through: :account_report_delivery_types
end
