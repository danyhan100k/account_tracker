# == Schema Information
#
# Table name: phone_numbers
#
#  id             :integer          not null, primary key
#  phoneable_id   :integer
#  phoneable_type :string(255)
#  number         :string(255)
#  number_type    :string(255)
#  notes          :string(255)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
# Indexes
#
#  index_phone_numbers_on_phoneable_type_and_phoneable_id  (phoneable_type,phoneable_id)
#

class PhoneNumber < ActiveRecord::Base
  belongs_to :phoneable, polymorphic: true
  #validates_presence_of :number
  #validates :number, format: { with: /\A\d\d\d-\d\d\d-\d\d\d\d\z/, message: "has invalid Format. ex) 847-777-7777" }
	validates :notes, length: { maximum: 255,
	    too_long: "%{count} characters is the maximum allowed" }
	    
  def name
    number
  end
end
