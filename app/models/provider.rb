# == Schema Information
#
# Table name: providers
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  npi_number :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Provider < ActiveRecord::Base
	has_many :account_providers
	has_many :accounts, through: :account_providers

	validates_presence_of :name #, :npi_number
	validates_length_of :name, :npi_number, maximum: 255
end
