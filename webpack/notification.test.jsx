const React = require('react');
const { createStore } = require('redux'); 
const { Provider } = require('react-redux');
const { rootSpecimenReducer } = require('./store');
const renderer = require('react-test-renderer');
const { Notification, } = require('./notification');
const { render } = require('enzyme');

function buildComponent(store) {
  return (
    <Provider store={store}>
			<Notification />
		</Provider>
  );
}

test('Notification ON snapshot test', () => {
	const store = createStore(rootSpecimenReducer, {
		notification: {
			open: true,
			message: 'Notification is turned ON!'
		}
	});
  const element = renderer.create(buildComponent(store));
	expect(element.toJSON()).toMatchSnapshot();
});

test('Notification OFF snapshot test', () => {
	const store = createStore(rootSpecimenReducer, {
		notification: {
			open: false,
			message: 'Notification is turned OFF!'
		}
	});
	const element = renderer.create(buildComponent(store));
	expect(element.toJSON()).toMatchSnapshot();
});

test('Notification is opened', () => {
	const store = createStore(rootSpecimenReducer, {
		notification: {
      open: true,
		}
	});

	const element = render(buildComponent(store)).find(".notification-container");
	expect(element.hasClass('open')).toEqual(true);
});

test('Notification is closed', () => {
	const store = createStore(rootSpecimenReducer, {
		notification: {
      open: false,
		}
	});

	const element = render(buildComponent(store)).find(".notification-container");
	expect(element.hasClass('open')).toEqual(false);
});

test('Notification displays its message', () => {
  const message = "Specimen updated successfully";
	const store = createStore(rootSpecimenReducer, {
		notification: {
      message
		}
	});

	const element = render(buildComponent(store)).find('.notification');
	expect(element.text()).toEqual(message);
});
