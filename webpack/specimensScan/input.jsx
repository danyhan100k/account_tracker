const React = require('react');

export const Input = ({ 
  id, 
  value, 
  type, 
  onFocus, 
  onBlur, 
  onChange, 
  focus
}) => {
  let input;
  onChange = onChange || (() => {});
  return (
    <input
      ref={ref => input = ref}
      id={id}
      autoFocus={focus}
      type={type}
      value={value}
      onFocus={onFocus}
      onBlur={onBlur}
      onChange={() => onChange(input.value)}
    />
  );
};
