const React = require('react');
const { Input } = require('./input');
const renderer = require('react-test-renderer');

test('Input snapshot test', () => {
	const component = renderer.create(
		<Input 
			id={0}
			value={'test'}
			type={'text'}
			focus={true}
			onFocus={() => {}}
			onBlur={() => {}}	
		/>
	);
	const tree = component.toJSON();
	expect(tree).toMatchSnapshot();
});