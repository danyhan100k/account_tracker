const React = require('react');
const { createStore } = require('redux');
const { Provider } = require('react-redux');
const { rootSpecimenReducer } = require('../store');
const { DateReceived } = require('./dateReceived');
const renderer = require('react-test-renderer');

test('DateReceived snapshot test', () => {
	const store = createStore(
		rootSpecimenReducer,
		{ dateReceived: '2017-07-07'}
	);
	const component = renderer.create(
		<Provider store={store}>
			<DateReceived />
		</Provider>
	);
	const tree = component.toJSON();
	expect(tree).toMatchSnapshot();
});