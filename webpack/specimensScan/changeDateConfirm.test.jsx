const React = require('react');
const { createStore } = require('redux');
const { Provider } = require('react-redux');
const { ChangeDateConfirm } = require('./ChangeDateConfirm');
const renderer = require('react-test-renderer');
const { render, mount, shallow } = require('enzyme');

jest.mock('../actionCreators.js', () => ({
  changeDateReceived: jest.fn()
}));

test('ChangeDateConfirm mode OPEN snapshot test', () => {
	const component = renderer.create(
		<Provider store={mockStore(true, { date_received: '2017-07-07' })}>
			<ChangeDateConfirm />
		</Provider>
	);
	expect(component.toJSON()).toMatchSnapshot();
});

test('ChangeDateConfirm mode OFF snapshot test', () => {
	const component = renderer.create(
		<Provider store={mockStore(false)}>
			<ChangeDateConfirm />
		</Provider>
	);
	expect(component.toJSON()).toMatchSnapshot();
});

test("props.open = true displays the modal", () => {
	const component = mount(
		<Provider store={mockStore(true)}>
			<ChangeDateConfirm />
		</Provider>
	);
	expect(component.find('.ug-modal-container').exists()).toEqual(true)
});

test("displays the specimen's date_received", () => {
  const date_received = '2017-07-07';
	const component = mount(
		<Provider store={mockStore(true, { date_received })}>
			<ChangeDateConfirm />
		</Provider>
	);
	expect(component.text()).toMatch(date_received);
});

test("props.open = false hides the modal", () => {
	const component = mount(
		<Provider store={mockStore(false)}>
			<ChangeDateConfirm />
		</Provider>
	);
	expect(component.find('.ug-modal-container').exists()).toEqual(false)
});

test("CHANGE_DATE_RECEIVED is dispatched when Ok button is clicked", () => {
  const specimen = { date_received: '2017-07-07' };
  const store = mockStore(true, specimen);
	const component = mount(
		<Provider store={store}>
			<ChangeDateConfirm />
		</Provider>
	);
  const { changeDateReceived } = require('../actionCreators.js');

  const returnValue = {};
  changeDateReceived.mockReturnValueOnce(returnValue);

  component.find(".ok").simulate("click");
  expect(changeDateReceived).toBeCalledWith(specimen);
  expect(store.dispatch.mock.calls[0][0]).toBe(returnValue);
});

// TODO:
// - Replace Cancel button test's expectation with a mock for an action creator
// - Actually create the action creators (bonus points for using TDD!)
// - Check how to unmock the actionCreators.js module after running all tests in this file

test("CHANGE_DATE_CANCELED is dispatched when Cancel button is clicked", () => {
  const store = mockStore(true);
	const component = mount(
		<Provider store={store}>
			<ChangeDateConfirm />
		</Provider>
	);
  component.find(".cancel").simulate("click");

  expect(store.dispatch).toBeCalledWith({
    type: 'CHANGE_DATE_CANCELED',
  });
});

function mockStore(open, specimen = {}) {
  const reducer = (state, action) => state;
	const store = createStore(reducer, {
		changeDateConfirm: {
			open,
			specimen,
		}
	});
  store.dispatch = jest.fn();

  return store;
};
