const React = require('react');
const { Input } = require('./input.jsx');
const { Autocomplete } = require('../autocomplete.jsx');

const Checkbox = ({ id, checked, onChange, onBlur }) => {
  let input;
  return (
    <input
      ref={ref => input = ref}
      id={id}
      type="checkbox"
      checked={checked}
      onBlur={onBlur}
      onChange={() => onChange(input.checked)}
    />
  );
};

const Error = ({ message }) => (
  <small className="error">{message}</small>
);

export const SpecimenForm = React.createClass({
  // TODO: Figure out what's causing the rerender
  // when this work-around is removed
  shouldComponentUpdate(nextProps) {
    return Object.keys(nextProps).some(prop =>
      this.props[prop] !== nextProps[prop]
    );
  },

  render() {
    let {
      specimen,
      onUpdateSpecimen,
      onChangeSpecimen,
      changeAndUpdateSpecimen,
      prefixes,
      focus,
      errors,
    } = this.props;

    const { submitted } = specimen;
    errors = errors || {};

    let originalOnChangeSpecimen = onChangeSpecimen;
    onChangeSpecimen = changeset => originalOnChangeSpecimen(changeset, specimen.id);

    return (
      <div className="row table-row">
        <form
          onSubmit={ev => {
            ev.preventDefault();
            onUpdateSpecimen(specimen);
          }}
        >

          <div className="col-md-2">
            <Input
              id={'date-received'}
              type={'date'}
              value={specimen.date_received}
              onBlur={() => onUpdateSpecimen(specimen)}
              onChange={date_received => onChangeSpecimen({ date_received })}
            />
            { submitted && errors.date_received && <Error message={errors.date_received} /> }
          </div>

          <div className="col-md-1">
            <Input
              id={'batch-number'}
              type={'text'}
              value={specimen.batch}
              onBlur={() => onUpdateSpecimen(specimen)}
              onChange={batch => onChangeSpecimen({ batch })}
            />
            { submitted && errors.batch && <Error message={errors.batch} /> }
          </div>

          <div className="col-md-1">
            <Autocomplete
              value={specimen.account_prefix}
              options={prefixes}
              onSelect={account_prefix => 
                changeAndUpdateSpecimen({ account_prefix }, specimen.id)
              }
            />
            { submitted && errors.account_prefix && <Error message={errors.account_prefix} /> }
          </div>

          <div className="col-md-2">
            <Input
              focus={focus}
              id={'requisition-number'}
              type={'text'}
              onBlur={() => onUpdateSpecimen(specimen)}
              value={specimen.requisition_number}
              onChange={requisition_number => onChangeSpecimen({ requisition_number })}
            />
            { submitted && errors.requisition_number && <Error message={errors.requisition_number} /> }
          </div>

          <div className="col-md-1">
            <Input
              id={'sample-type'}
              type={'text'}
              value={specimen.sample_type_id}
              onBlur={() => onUpdateSpecimen(specimen)}
              onChange={sample_type_id => onChangeSpecimen({ sample_type_id })}
            />
            { submitted && errors.sample_type_id && <Error message={errors.sample_type_id} /> }
          </div>

          <div className="col-md-1">
            <label>
              <Checkbox
                id={'processable'}
                checked={specimen.is_processable}
                onBlur={() => onUpdateSpecimen(specimen)}
                onChange={is_processable => onChangeSpecimen({ is_processable })}
              />
            </label>
          </div>

          <div className="col-md-2">
            {
              specimen.created_at &&
              new Date(specimen.created_at).toLocaleString() ||
              "Not created yet"
            }
          </div>

          <div className="col-md-2">
            <Input
              id={'created-by'}
              type={'text'}
              value={specimen.created_by}
              onBlur={() => onUpdateSpecimen(specimen)}
              onChange={created_by => onChangeSpecimen({ created_by })}
            />
            { submitted && errors.created_by && <Error message={errors.created_by} /> }
          </div>

          <button style={{ display: 'none' }}></button>
        </form>
      </div>
    );
  }
});
