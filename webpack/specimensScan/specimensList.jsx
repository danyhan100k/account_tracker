const React = require('react');
const { connect } = require('react-redux');
const { SpecimenForm } = require('./specimenForm.jsx');
const { 
  saveSpecimen,
  onChangeSpecimen,
  onUpdateSpecimen,
  changeAndUpdateSpecimen
} = require('../actionCreators');

let SpecimensList = ({
  specimens,
  focusedSpecimen,
  onChangeSpecimen,
  onUpdateSpecimen,
  changeAndUpdateSpecimen,
  errors,
  prefixes,
}) => {
  return(
    <div className="table">
      <div className="row table-header">
        <div className="col-md-2">Date Received</div> { /* default: selected date  */ }
        <div className="col-md-1">Batch number</div> { /* default: 1  */ }
        <div className="col-md-1">Prefix</div> { /* default: the same as the above row  */ }
        <div className="col-md-2">Requisition number</div>
        <div className="col-md-1">Sample type</div> { /* default: the same as the above row  */ }
        <div className="col-md-1">Processable</div> { /* default: true  */ }
        <div className="col-md-2">Created at</div>
        <div className="col-md-2">Current user</div> { /* default: host  */ }
      </div>

      {
        specimens.map((specimen, index) => (
          <SpecimenForm
            key={specimen.id}
            errors={errors[specimen.id]}
            focus={specimen.id === focusedSpecimen.id}
            specimen={specimen}
            prefixes={prefixes}
            onChangeSpecimen={onChangeSpecimen}
            onUpdateSpecimen={onUpdateSpecimen}
            changeAndUpdateSpecimen={changeAndUpdateSpecimen}
          />
        ))
      }
  </div>)
};

SpecimensList.propTypes = {
  specimens: React.PropTypes.array,
  focusedSpecimen: React.PropTypes.object,
  errors: React.PropTypes.object,
  prefixes: React.PropTypes.array,
  onChangeSpecimen: React.PropTypes.func,
  onUpdateSpecimen: React.PropTypes.func,
};

const mapStateToProps = ({
  specimensList,
  prefixes,
  errorsMap
}) => Object.assign(
  {},
  specimensList,
  {
    prefixes,
    errors: errorsMap,
  }
);

const mapDispatchToProps = dispatch => ({
  onChangeSpecimen: onChangeSpecimen(dispatch),
  onUpdateSpecimen: onUpdateSpecimen(dispatch),
  changeAndUpdateSpecimen: changeAndUpdateSpecimen(dispatch)
});

SpecimensList = connect(
  mapStateToProps, 
  mapDispatchToProps
)(SpecimensList);

export { SpecimensList };
