const React = require('react');
const { connect } = require('react-redux');
const { Input } = require('./input.jsx');

let DateReceived = ({ dateReceived, onSubmit, onChange }) => {
  return (
    <form
      onSubmit={ev => {
        ev.preventDefault();
        onSubmit(dateReceived);
      }}
    > 
      <h4>Select Date</h4>
      <Input
        type="date"
        value={dateReceived}
        onChange={onChange}
      />
      <button 
        className="ui primary basic button large">
        Search
      </button>
    <br/>
    <br/>
    </form>

  );
};

DateReceived.propTypes = {
  dateReceived: React.PropTypes.string,
  onSubmit: React.PropTypes.func,
  onChange: React.PropTypes.func,
};

const mapStateToProps = ({ dateReceived }) => ({ dateReceived });
const mapDispatchToProps = dispatch => ({
  onSubmit: date => dispatch({ 
    type: "CHANGE_DATE_RECEIVED", 
    date,
  }),
  onChange: date => dispatch({
    type: "SET_DATE_RECEIVED",
    date,
  }),
});

DateReceived = connect(
  mapStateToProps, 
  mapDispatchToProps
)(DateReceived);

export { DateReceived };
