const React = require('react');
const { createStore } = require('redux');
const { Provider } = require('react-redux');
const { rootSpecimenReducer } = require('../store');
const { SpecimenForm } = require('./specimenForm');
const renderer = require('react-test-renderer');

const specimen = {
		id: 'test-key',
		batch: 1,
		date_received: '2017-07-07',
		account_prefix: 'AB',
		requisition_number: '77777',
		sample_type_id: 7,
		is_processable: true,
		created_by: "testUser1"
	};

test('SpecimenForm snapshot test', () => {
	const component = renderer.create(
		<SpecimenForm 
			key={'test-key'}
			specimen={specimen}
			onUpdateSpecimen={() => {}}
			onChangeSpecimen={() => {}}
			changeAndUpdateSpecimen={() => {}}
			prefixes={['AA', 'AB','AC','AD']}
			focus={true}
		/>
	);
	const tree = component.toJSON();
	expect(tree).toMatchSnapshot();
});

test('SpecimenForm with Errors snapshot test', () => {
	const errors = {
		'test-key': { 
			'date_received': "can't be blank",
			'batch': "can't be blank",
			'requisition_number': "can't be blank",
			'sample_type_id': "can't be blank",
			'account_prefix': "can't be blank",
			'created_by': "can't be blank"
		}
	};
	const component = renderer.create(
		<SpecimenForm 
			key={'test-key'}
			specimen={Object.assign({}, specimen, { submitted: true })}
			onUpdateSpecimen={() => {}}
			onChangeSpecimen={() => {}}
			changeAndUpdateSpecimen={() => {}}
			prefixes={['AA', 'AB','AC','AD']}
			focus={true}
			errors={errors[specimen.id]}
		/>
	);
	const tree = component.toJSON();
	expect(tree).toMatchSnapshot();
});