// - The first screen has only an input for the requisition number, which is focused
// - After the user presses return, a request is sent to the back-end to search for a specimen with that requisition number
// - If none is present, it's created a new one (with the received_date as today)
// - A specimen is returned as a JSON from the back-end
// - After the front-end gets the response, it shows a form with some fields pre-populated according to the specimen's data
// - The user can click on "Submit" to update the specimen 
// - If any error occurs (validation or unexpected error), show some feedback to the user
// - If successful, show some feedback (toast), reset the form to the default state (only the requisition number input) and add the just created/updated specimen to a table (which will work as a search result as well)
//
//
// TODO:
// - [x] Fetch API for initial specimens (with today's date)
// - [x] Allow user to update specimen if it already exists
// - [x] Display created at in the table
// - [x] Edit in-place specimen fields (hide input when not focused)
// - [x] Add some validation
// - [x] Handle non expected errors when accessing API
// - [x] Ask user if he wants to override specimen in case there's already a specimen with the given requisition number
// - [x] Show a notification for creating/updating successfully
// - [x] Show a spinner while loading data from the back-end
// - [x] Ask for confirmation before navigating to existing specimen's date (after conflict in requisition number) using HTML elements
// - [x] Focus on existing specimen after changing date received
// - [x] Refactor reducers so we always have a focused specimen (instead of two different rules for deciding in which one to focus on)
// - [x] Integrate validations on the back-end with validations on the front-end to provide better UX
// - [x] Displaying actual account prefixes for the select
// - [x] Fix performance when updating a specimen in a long list (ALMOST THERE!)
// - [x] Fix update prefix
// - [ ] Let the user know if he has unsaved changes (display a prompt, for example)
// - [ ] Ask user whether he really wants to update the date_received for a specimen
// - [ ] Refactoring EVERYTHING

const ReactDOM = require('react-dom');
const React = require('react');
const { Provider } = require('react-redux');
const specimenFormElement = document.getElementById('specimen-form');
const { store } = require('./store.jsx');
const { SpecimenForm } = require('./specimensScan/specimenForm.jsx');
const { SpecimensList } = require('./specimensScan/specimensList.jsx');
const { DateReceived } = require('./specimensScan/dateReceived.jsx');
const { Notification } = require('./notification.jsx');
const { Spinner } = require('./spinner.jsx');
const { ChangeDateConfirm } = require('./specimensScan/changeDateConfirm.jsx');

const Root = React.createClass({
  render() {
    return (
      <div>
        <Notification />
        <Spinner />
        <ChangeDateConfirm />
        <DateReceived />
        <SpecimensList />
      </div>
    );
  }
});

if (specimenFormElement) {
  store.dispatch({ type: 'FETCH_PREFIXES' });
  store.dispatch({ type: "CHANGE_DATE_RECEIVED", date: store.getState().dateReceived });

  ReactDOM.render(
    <Provider store={store}>
      <Root />
    </Provider>,
    specimenFormElement
  );
}
