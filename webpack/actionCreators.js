export const saveSpecimen = specimen => (dispatch, getState) => {
  dispatch({ type: "SAVE_SPECIMEN", specimen });

  let actionToDispatch;
  let request;

  if (specimen.created_at) {
    actionToDispatch = "UPDATED_SPECIMEN";
    request = fetch(`/api/v1/specimens/${specimen.id}`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ specimen }),
    });
  } else {
    actionToDispatch = "CREATED_SPECIMEN";
    request = fetch(`/api/v1/specimens`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ specimen }),
    });
  }

  request.then(response => {
    if (response.ok) {
      return response.json();
    }

    throw response;
  }).then(newSpecimen =>
    dispatch({
      type: actionToDispatch,
      specimen: newSpecimen,
      oldId: specimen.id
    })
  ).catch(response => {
    if (response.status === 422) {
      response.json().then(errors => 
        dispatch({
          type: 'VALIDATION_ERROR',
          errors: errors.error,
          id: specimen.id,
        })
      );
    } else if (response.status === 409) {
      response.json().then(result => 
        dispatch({
          type: 'SPECIMEN_ALREADY_EXISTS',
          specimen: result.specimen,
        })
      );
    } else {
      dispatch({ type: 'UNKNOWN_ERROR', error: response.statusText });
    }
  });
};

export const onChangeSpecimen = dispatch => (specimen, id) => dispatch({
  type: 'CHANGE_SPECIMEN',
  specimen,
  id
});

export const onUpdateSpecimen = dispatch => specimen => dispatch(saveSpecimen(specimen));

export const changeAndUpdateSpecimen = (dispatch) => (changeset, id) => {
  onChangeSpecimen(dispatch)(changeset, id);
  dispatch((dispatch, getState) => {
    const specimen = getState().specimensList.specimens.filter(s => s.id === id)[0];
    onUpdateSpecimen(dispatch)(specimen);
  });
};

export const setPrefixes = (prefixes) => ({
  type: "SET_PREFIXES",
  prefixes,
});
