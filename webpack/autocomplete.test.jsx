const React = require('react');
const renderer = require('react-test-renderer');
const { Autocomplete } = require('./autocomplete');
const { SpecimenForm } = require('./specimensScan/specimenForm');  
const { Input } = require('./specimensScan/input');
let { changeAndUpdateSpecimen } = require('./actionCreators');
const { render, mount, shallow } = require('enzyme');

test('Autocomplete snapshot test', () => {
	const component = renderer.create(
		<Autocomplete 
			value={"testing autocomplete component"}
			options={['AA', 'AB', 'AC', 'AH']}
		/>
	);
	const tree = component.toJSON();
	expect(tree).toMatchSnapshot();
});

it("props.value is set as the input's value", () => {
  const value = "option 2";
  const component = render(
    <Autocomplete 
      value={value}
      options={[]}
    />
  );

  expect(component.find('input').prop('value')).toEqual(value);
});

it("renders a list with each element in the options", () => {
  const component = mount(
    <Autocomplete
      options={["option 1", "option 2"]}
    />
  );

  expect(component.find('ul.options li').length).toEqual(2);
  expect(component.find('ul.options li').at(0).text()).toEqual("option 1");
  expect(component.find('ul.options li').at(1).text()).toEqual("option 2");
});

it("shows options when input is focused", () => {
  const component = mount(
    <Autocomplete
      options={["option 1", "option 2"]}
    />
  );

  component.find("input").simulate("focus");
  component.update();

  expect(component.find('ul.options').hasClass('show')).toEqual(true);
});

function changeValue(characters, element) {
  // https://github.com/airbnb/enzyme/issues/364#issuecomment-252553369
  element.node.value = characters;
  element.simulate("change", element);
}

it("filters options based on input", () => {
  const component = mount(
    <Autocomplete
      options={["abc", "ade"]}
    />
  );

  const input = component.find("input");
  input.simulate("focus");
  changeValue("ab", input);

  expect(component.find('ul.options li').length).toEqual(1);
  expect(component.find('ul.options li').at(0).text()).toEqual("abc");
}); 

it("calls onSelect with clicked option", () => {
  const onSelect = jest.fn();

  const component = mount(
    <Autocomplete
      options={["abc", "ade"]}
      onSelect={onSelect}
    />
  );

  component.find("input").simulate("focus");
  component.update();
  component.find('ul.options li').at(0).simulate('click');
  expect(onSelect).toBeCalledWith("abc");
});

it("blurring out of the input removes options list", () => {
  const component = mount(
    <Autocomplete
      options={["abc", "ade"]}
    />
  );
  const input = component.find('input');
  input.simulate('focus');
  component.update();
  expect(component.find('ul').hasClass('show')).toEqual(true);
  input.simulate('blur');
  component.update();
  expect(component.find('ul').hasClass('show')).toEqual(false);
});
