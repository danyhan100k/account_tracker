const React = require('react');
const renderer = require('react-test-renderer');
const { createStore } = require('redux');
const { Provider } = require('react-redux');
const { Spinner } = require('./spinner');
const { render } = require('enzyme'); 

const reducer = (state) => state;
const showingStore = createStore(reducer, {
  spinner: true
});

const hidingStore = createStore(reducer, {
  spinner: false
});

function buildComponent(store) {
  return (
    <Provider store={store}>
      <Spinner />
    </Provider>
  );
}

test('Spinner ON snapshot test', () => {	
	const component = renderer.create(
		buildComponent(showingStore)
	);
	expect(component.toJSON()).toMatchSnapshot();
});

test('Spinner OFF snapshot test', () => {	
	const component = renderer.create(
		buildComponent(hidingStore)
	);
	expect(component.toJSON()).toMatchSnapshot();
});

test('if props.spinner is true then the spinner is showing', () => {
	const component = render(
		buildComponent(showingStore)
	);
	expect(component.find('.spinner-container').length).toEqual(1);
});

test("if props.spinner is false, don't render anything", () => {
	const component = render(
		buildComponent(hidingStore)
	);
	expect(component.find('.spinner-container').length).toEqual(0);
});
