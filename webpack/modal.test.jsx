const React = require('react');
const { Modal } = require('./modal');
const renderer = require('react-test-renderer');
const { shallow } = require('enzyme');
//writing snapshot test

// what Jest snapshot does is
// 1) renders your component out 
// 2) it dumps that into a file
// 3) compares against that file in the future

test('Modal snapshot test', () => {
	const component = renderer.create(<Modal />);
	const tree = component.toJSON();
	expect(tree).toMatchSnapshot(); 
});

test('Modal renders children when passed in', () => {
	let Component = shallow(
		<Modal>
			<h4>Existing Requisition Number found!</h4>
		</Modal>
	);
	expect(
		Component.contains(
			<h4>Existing Requisition Number found!</h4>
		)
	).toEqual(true);
});
