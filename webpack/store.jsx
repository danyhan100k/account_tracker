const { createStore, combineReducers, applyMiddleware } = require('redux');
const { v4 } = require('node-uuid');

function generateId() {
  return v4();
}

function generateNewSpecimen(date_received) {
  return {
    date_received,
    id: generateId(),
    batch: 1,
    account_prefix: "",
    requisition_number: "",
    sample_type_id: "",
    is_processable: true,
    created_by: "",
  };
}

function copySpecimen(last) {
  return {
    id: generateId(),
    date_received: last.date_received,
    batch: last.batch,
    account_prefix: last.account_prefix,
    requisition_number: "",
    sample_type_id: last.sample_type_id,
    is_processable: last.is_processable,
    created_by: last.created_by,
  };
}

export const rootSpecimenReducer = combineReducers({
  specimensList,
  dateReceived,
  errorsMap,
  notification,
	spinner,
  changeDateConfirm,
  prefixes,
});

const thunk = require('redux-thunk').default;

export const store = createStore(rootSpecimenReducer, applyMiddleware(thunk));

function specimensList(state = {
  specimens: [],
  focusedSpecimen: {}
}, action) {
  switch(action.type) {
    case 'SET_SPECIMENS':
      const newSpecimens = specimens(state.specimens, action);
      return {
        specimens: newSpecimens,
        focusedSpecimen: action.focusedSpecimen || newSpecimens[newSpecimens.length - 1],
      }
    case 'CREATED_SPECIMEN':
      let specimensToSet = specimens(state.specimens, action);
      let focusedSpecimenToSet = specimensToSet[specimensToSet.length - 1]
      return Object.assign({}, state, {
        specimens: specimensToSet,
        focusedSpecimen: focusedSpecimenToSet
      });
    case 'CHANGE_SPECIMEN':
    case 'SAVE_SPECIMEN':
    case 'UPDATED_SPECIMEN':
    case 'CHANGE_DATE_RECEIVED':
      state = Object.assign({}, state, {
        specimens: specimens(state.specimens, action),
      });
      return state;     
    default: return state;
  }
}

function specimens(
  state = [generateNewSpecimen(formatDate(new Date()))],
  action
) {
  switch(action.type) {
    case 'CHANGE_SPECIMEN':
      return state.map(s => {
        if (s.id == action.id) {
          return specimen(s, action);
        }

        return s;
      });
    case 'SAVE_SPECIMEN':
      return state.map(specimen => {
        if (specimen.id == action.specimen.id) {
          return Object.assign({}, specimen, { submitted: true });
        }

        return specimen;
      });
    case 'UPDATED_SPECIMEN':
      return state.map(specimen => {
        if (specimen.id === action.oldId) {
          return action.specimen;
        }

        return specimen;
      });
    case 'CREATED_SPECIMEN':
      return state.map(specimen => {
        if (specimen.id === action.oldId) {
          return action.specimen;
        }

        return specimen;
      }).concat([copySpecimen(action.specimen)]);
    case 'CHANGE_DATE_RECEIVED':
      fetch(`/api/v1/specimens?date_received=${action.date}`)
      .then(response => {
        return response.json();
      })
      .then(specimens => 
        store.dispatch({
          type: 'SET_SPECIMENS',
          specimens,
          date: action.date,
          focusedSpecimen: action.focusedSpecimen,
        })
      );
      return state;
    case 'SET_SPECIMENS':
      const last = action.specimens[action.specimens.length - 1];
      return action.specimens.concat([
        last ? copySpecimen(last) : generateNewSpecimen(action.date)
      ]);
    default: 
      return state;
  };
};

function specimen(state = {}, action) {
  switch(action.type) {
    case 'CHANGE_SPECIMEN':
      return Object.assign({}, state, action.specimen);
    default:
      return state;
  }
}

function dateReceived(state = formatDate(new Date()), action) {
  switch(action.type) {
    case 'SET_DATE_RECEIVED':
      return action.date;
    case 'CHANGE_DATE_RECEIVED':
      return action.date;
    default:
      return state;
  }
}

function formatDate(date) {
  const day = leftPad(date.getDate(), "0", 2);
  const month = leftPad(date.getMonth() + 1, "0", 2);
  const year = date.getFullYear();

  return `${year}-${month}-${day}`;
}

function leftPad(string, padding, length) {
  string = string.toString();
  let acc = "";
  for (let i = 0; i < length - string.length; i++) {
    acc += padding;
  }

  return acc + string;
}

function errorsMap(state = {}, action) {
  switch(action.type) {
    case 'VALIDATION_ERROR':
      return Object.assign({}, state, { [action.id]: action.errors });
    case 'UNKNOWN_ERROR':
      alert(action.error);
      return state;
    case 'CHANGE_SPECIMEN':
      return Object.assign({}, state, {
        [action.id]: Object.assign(
          {},
          state[action.id],
          validate(action.specimen)
        )
      });
    default:
      return state;
  }
}

function validate(specimen) {
  let errors = {};

  errors = validateRequired(errors, specimen, 'date_received');
  errors = validateRequired(errors, specimen, 'batch');
  errors = validateRequired(errors, specimen, 'requisition_number');
  errors = validateRequired(errors, specimen, 'sample_type_id');
  errors = validateRequired(errors, specimen, 'account_prefix');
  errors = validateRequired(errors, specimen, 'created_by');

  return errors;
}

function validateRequired(errors, obj, key) {
  if (obj.hasOwnProperty(key)) {
    if (obj[key]) {
      return Object.assign({}, errors, { [key]: undefined });
    } else {
      return Object.assign({}, errors, { [key]:  "can't be blank" });
    }
  }

  return errors;
}

function notification(state = { open: false, message: "" }, action) {
  switch(action.type) {
    case 'CREATED_SPECIMEN':
      hideNotification();
      return {
        message: "Created specimen successfully",
        open: true,
      };
    case 'UPDATED_SPECIMEN':
      hideNotification();
      return {
        message: "Updated specimen successfully",
        open: true,
      };
    case 'HIDE_NOTIFICATION':
      return Object.assign({}, state, { open: false });
    default: return state;
  }
}

function hideNotification() {
  setTimeout(() => store.dispatch({ type: "HIDE_NOTIFICATION" }), 1700);
}

function spinner(state = false, action) {
  switch(action.type) {
    case 'CHANGE_DATE_RECEIVED':
      return true;
    case 'SET_SPECIMENS':
    case 'UNKNOWN_ERROR':
      return false;
    default: return state;
  }
}

export function changeDateConfirm(
  state = { specimen: null, open: false }, 
  action
) {
  switch(action.type) {
    case 'SPECIMEN_ALREADY_EXISTS':
      return {
        specimen: action.specimen,
        open: true,
      };
    case 'CHANGE_DATE_RECEIVED':
    case 'CHANGE_DATE_CANCELED':
      return {
        specimen: null,
        open: false,
      };
    default: return state;
  }
}

function prefixes(state = [], action) {
  switch(action.type) {
    case 'FETCH_PREFIXES':
      fetch('/api/v1/accounts.json')
        .then(response => response.json())
        .then(accounts =>
          store.dispatch(
            setPrefixes(accounts.map(account => account.prefix))
          )
        );
      return state;
    case 'SET_PREFIXES':
      return [].concat(action.prefixes);
    default: return state;
  }
}
