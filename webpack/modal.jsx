const React = require('react');

export const Modal = ({ children }) => (
  <div className="ug-modal-container">
    <div className="ug-modal">
      { children }
    </div>
  </div>
);
