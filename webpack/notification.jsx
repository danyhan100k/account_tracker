const React = require('react');
const { connect } = require('react-redux');

const NotificationPres = ({ open, message }) => (
  <div className={`notification-container${open ? " open" : ""}`}>
    <span className="notification">{message}</span>
  </div>
);

NotificationPres.propTypes = {
  open: React.PropTypes.bool,
  message: React.PropTypes.string,
};

const mapStateToProps = ({ notification }) => notification;
const Notification = connect(mapStateToProps)(NotificationPres);

export { Notification };
