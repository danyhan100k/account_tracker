const React = require('React');
const { Input } = require('./specimensScan/input.jsx');

export const Autocomplete = React.createClass({
  componentWillMount() {
    this.value = this.props.value;
  },

  componentWillReceiveProps(nextProps) {
    this.value = nextProps.value;
  },

  render() {
    const { onSelect, options } = this.props;
    this.value = this.value || "";

    return (
      <div className="autocomplete">
        <Input
          type="text"
          value={this.value}
          onChange={newValue => {
            this.value = newValue;
            this.forceUpdate();
          }}
          onFocus={() => this.showOptions = true}
          onBlur={() => this.showOptions = false}
        />

        <ul className={"options" + (this.showOptions ? " show" : "")}>
         {
           options.filter(option =>
             option.toLowerCase().startsWith(this.value.toLowerCase())
           ).map(option =>
             <li className="option" key={option} onClick={() => 
               onSelect(option)
             }>{option}</li>
           )
         }
        </ul>

      </div>
    );
  }
});

Autocomplete.propTypes = {
  onSelect: React.PropTypes.func,
  options: React.PropTypes.array,
  value: React.PropTypes.string,
};
