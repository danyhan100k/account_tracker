const { setPrefixes } = require('./actionCreators');

describe("actions", () => {
  it("should create an action to set the prefixes", () => {
    const prefixes = ["AA", "AB"];

    const expected = {
      type: "SET_PREFIXES",
      prefixes
    };

    expect(setPrefixes(prefixes)).toEqual(expected);
  });
});
